<? /* Template Name: Member Confirmation */ ?>

<?php get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <main class="generic">
        <div class="grid-container">
            <div class="grid-x grid-margin-x">
                <div class="small-12 cell">
                    <h1 class="generic__page-title">
                        <?php if(isset($_GET['form'])): ?>
                            Thanks for requesting the report!
                        <?php else: ?>
                            <?php the_title(); ?>
                        <?php endif; ?>
                    </h1>

                    <div class="generic__content">
                        <?php if(isset($_GET['form'])): ?>
                            We'll be in touch shortly.
                        <?php else: ?>
                            <?php the_content() ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php endwhile; else : ?>

    <?php endif; ?>

<?php get_footer(); ?>
