<? /* Template Name: Contact Page */ ?>

<?php get_header(); ?>

    <div class="o-contact-page">
        <div class="o-contact-page__inner">
            <div class="grid-container">
                <div class="grid grid-x">
                    <div class="cell small-12 medium-6">
                        <div class="c-small-title">
                            CONTACT US
                        </div>

                        <h1>
                            <?php the_field('heading') ?> 
                        </h1>
                    </div>

                    <div class="cell small-12 medium-6">
                        <?php if ($id = get_field('gravity_form_id')): ?>
                            <div class="o-contact-page__form s-gravity-form s-dropkick s-contact-form js-contact-form js-gravity-form">
                                <?= do_shortcode('[gravityform id="' . $id . '" title="false" description="false" ajax="true"]') ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>