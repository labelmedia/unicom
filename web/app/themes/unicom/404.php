<?php get_header(); ?>

    <section class="o-404 o-section o-section--fixed o-section--navy js-snap-scroll">

        <h2 class="o-404__heading o-section__heading o-section__heading--primary">404</h2>

        <div class="o-404__message">We're sorry, this page doesn't exist.</div>

    </section>

<?php
    get_footer();
