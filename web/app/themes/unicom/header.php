<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php wp_title() ?>
    </title>    

    <script src="//code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link href="//fonts.googleapis.com/css2?family=Bree+Serif&family=Open+Sans&display=swap" rel="stylesheet">

    <?php if (getenv('WP_ENV') == 'production'): ?>
        <!-- Google Tag Manager -->
        <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5FF29Z4');
        </script>
        <!-- End Google Tag Manager -->
    <?php endif; ?>

    <?php wp_head() ?>
</head>
<body <?php body_class('js-body') ?>>

<?php if (getenv('WP_ENV') == 'production'): ?>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FF29Z4" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif; ?>

<header class="o-header utl-sunshine">
    <div class="grid-container full">
        <div class="grid grid-x align-center">
            <div class="cell small-12">
                <div class="grid-container full">
                    <div class="grid grid-x align-justify">
                        <div class="cell small-5 medium-auto o-header__logo medium-order-3">
                            <a href="<?= home_url() ?>" class="c-logo c-logo--header">
                                <img src="<?= get_template_directory_uri() ?>/dist/img/unicom-logo.svg" alt="Unicom">
                            </a>
                        </div>

                        <div class="cell shrink medium-order-2">
                            <div class="grid-container full">
                                <div class="grid grid-x align-middle">
                                    <div class="cell shrink">
                                        <a class="c-account-icon js-member-icon" data-target="login" href="<?= home_url('/members') ?>">
                                            Members Area
                                        </a>
                                    </div>

                                    <div class="cell shrink">
                                        <a href="#" class="c-menu-icon js-menu-icon">
                                            <span class="c-menu-icon__inner js-menu-icon__inner"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cell small-12 medium-shrink medium-order-1">
                            <div class="o-header__button-container">
                                <a href="<?= home_url('/unicom-insights') ?>" class="c-button c-button--block c-button--alpha">
                                    Join UNICOM
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('partials/components/menu-login'); ?>
    <?php get_template_part('partials/components/menu-register'); ?>
    <?php get_template_part('partials/components/menu-navigation'); ?>
</header>

<a class="c-scroll-to-top js-scroll-up"></a>