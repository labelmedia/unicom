<footer class="o-footer utl-sunshine">
    <?php if (have_rows('footer_logos', 'options')): ?>
    <div class="o-footer-logos">
        <div class="o-footer-logos__grid-container">
            <div class="o-footer-logos__grid js-footer-logos">
                <?php while(have_rows('footer_logos','options')): the_row(); ?>
                <div class="o-footer-logos__logo">
                    <?php

                    $image = get_sub_field('image');

                    if( !empty( $image ) ): ?>
                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="grid-container grid grid-x align-center">
        <div class="cell shrink">
            <div class="c-logo c-logo--footer">
                <img src="<?= get_template_directory_uri() ?>/dist/img/unicom-logo.svg" alt="Unicom">
            </div>
        </div>
    </div>

    <?php if (have_rows('footer_social_icons', 'options')): ?>
    <div class="grid-container o-footer__social-icons">
        <div class="grid grid-x align-center">
            <div class="small-12 medium-6 large-4">
                <div class="grid-container full">
                    <div class="grid grid-x align-justify">
                        <?php while (have_rows('footer_social_icons', 'options')): the_row(); ?>
                            <div class="cell shrink">
                                <a target="_blank" href="<?php the_sub_field('url') ?>" class="c-social-icon c-social-icon--<?php the_sub_field('css_modifier') ?>" style="background-image: url('<?php the_sub_field('image') ?>')"></a>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if (have_rows('footer_nav', 'options')): ?>
    <div class="grid-container o-footer__nav">
        <div class="grid grid-x align-center grid-margin-x">
            <?php while (have_rows('footer_nav', 'options')): the_row(); ?>
            <div class="cell small-12 medium-shrink">
                <?php
                $link = get_sub_field('link');

                if( $link ):
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                <?php endif; ?>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>

    <div class="grid-container">
        <div class="grid grid-x">
            <div class="cell small-12 text-center o-footer__copyright">
                @<?= Date('Y') ?> UNICOM All rights reserved
            </div>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>

</body>
</html>
