<? /* Template Name: Insight Report Landing Page */ ?>

<?php get_header(); ?>

    <div class="student-persona-research">
        <?php
            if(have_posts()) {
                while (have_posts()) {
                    the_post();

                    the_content();
                }
            };
        ?>

        <div class="student-persona-research__form-wrap">
            <div class="grid-container">
                <div class="grid-x grid-margin-x">
                    <div class="small-12 medium-6 cell">
                        <?php $report_image = get_field('report_image'); ?>

                        <?php if($report_image): ?>
                            <div class="student-persona-research__form-hero">
                                <?php echo wp_get_attachment_image($report_image['ID'], 'large'); ?>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="small-12 medium-6 cell">
                        <h2 class="student-persona-research__form-title">
                            <?php if(get_field('form_title')): ?>
                                <?php the_field('form_title'); ?>
                            <?php else: ?>
                                Download our report
                            <?php endif; ?>
                        </h2>

                        <?php if ($id = get_field('gravity_form_id')): ?>
                            <div class="s-gravity-form s-contact-form js-contact-form js-gravity-form">
                                <?= do_shortcode('[gravityform id="' . $id . '" title="false" description="false" ajax="true"]') ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
