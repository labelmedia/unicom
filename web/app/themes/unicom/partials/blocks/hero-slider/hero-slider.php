<div class="o-hero-slider js-hero js-hero__widget" data-aos-duration="300" data-aos="fade-up">
    <div class="grid-container full grid grid-x">
        <div class="cell small-12">
            <div class="o-hero-slider__container">
                <?php if (have_rows('hero_slider')): ?>
                    <div class="js-hero-slider c-hero-slide">
                        <?php while (have_rows('hero_slider')): the_row();  ?>
                            <div class="c-hero-slide__slide">
                                <?php if (have_rows('background_image')): while (have_rows('background_image')): the_row(); ?>
                                    <?php $image = get_sub_field('desktop'); ?>

                                    <?php if( !empty( $image ) ): ?>
                                        <div class="c-hero-slide__cover c-hero-slide__background show-for-medium" style="background-image:url(<?= esc_url($image['url']) ?>)"></div>
                                    <?php endif; ?>

                                    <?php $image = get_sub_field('mobile'); ?>

                                    <?php if( !empty( $image ) ): ?>
                                        <div class="c-hero-slide__cover c-hero-slide__background show-for-small-only" style="background-image:url(<?= esc_url($image['url']) ?>)"></div>
                                    <?php endif; ?>

                                    <?php unset($image); ?>
                                <?php endwhile; endif; ?>

                                <div class="c-hero-slide__cover o-hero-slide__content">
                                    <div class="grid-container full height-100">
                                        <div class="grid grid-x height-100 align-middle align-center">
                                            <div class="cell small-11">
                                                <?php if (is_singular('case-studies')): ?>
                                                    <div class="c-hero-slide__sub-heading utl-color-beta">
                                                        CASE STUDY
                                                    </div>

                                                    <div class="c-hero-slide__heading">
                                                        <?php the_title() ?>
                                                    </div>
                                                <?php else: ?>
                                                    <?php if ($subHeading = get_sub_field('sub_heading')): ?>
                                                        <div class="c-hero-slide__sub-heading utl-color-beta">
                                                            <?= $subHeading ?>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if ($heading = get_sub_field('heading')): ?>
                                                        <div class="c-hero-slide__heading">
                                                            <?= $heading ?>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if ($copy = get_sub_field('copy')): ?>
                                                        <div class="c-hero-slide__sub-heading">
                                                            <?= $copy ?>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php
                                                    $link = get_sub_field('cta');

                                                    if( $link ):
                                                        $link_url = $link['url'];
                                                        $link_title = $link['title'];
                                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                                    ?>

                                                    <a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>" class="c-button c-button--alpha">
                                                        <?= esc_html( $link_title ); ?>
                                                    </a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if (get_field('has_arrow')): ?>
        <?php get_template_part('partials/components/hero-arrow') ?>
    <?php endif; ?>
</div>

<?php if (get_field('has_widget')): ?>
    <?php get_template_part('partials/components/widget-hero') ?>
<?php endif; ?>
