<div class="o-related-posts js-posts-slider">
    <div class="grid-container">
        <div class="grid grid-x">
            <div class="cell small-12">
                <div class="o-related-posts__slider-container">
                    <h5 class="o-related-posts__heading">
                        Related Insights
                    </h5>

                    <?php
                     $relatedPosts = new WP_Query([
                        'post_type' => ['case-studies'],
                        'posts_per_page' => -1
                     ]);
                    ?>

                    <?php if ($relatedPosts->have_posts()): ?>
                    <div class="o-related-posts__slider">
                        <div class="js-posts-slider__slide swiper-container container-draggable">
                            <div class="swiper-wrapper">
                                <?php while ($relatedPosts->have_posts()): $relatedPosts->the_post(); $postId = get_the_ID() ?>
                                    <div class="swiper-slide">
                                        <a class="c-post-card" href="<?= the_permalink($postId) ?>" style="background-image: url('<?php the_field('preview_image', $postId) ?>')">
                                            <div class="c-post-card__inner">
                                                <h4 class="c-post-card__heading">
                                                    <?= get_the_title($postId) ?>
                                                </h4>

                                                <div class="c-post-card__taxonomy">
                                                    CASE STUDY
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="o-related-posts__progress-container">
                        <div class="grid-container full">
                            <div class="grid grid-x align-center">
                                <div class="cell small-12 medium-10">
                                    <div class="c-slider-progress">
                                        <div class="c-slider-progress__cta">
                                            <a href="#" class="c-slider-progress__slider-nav c-slider-progress__slider-nav--previous js-posts-slider__prev">
                                                Previous
                                            </a>

                                            <a href="#" class="c-slider-progress__slider-nav c-slider-progress__slider-nav--next js-posts-slider__next">
                                                Next
                                            </a>
                                        </div>

                                        <div class="c-slider-progress__bar-container">
                                            <div class="c-slider-progress__bar-full">
                                                <span class="c-slider-progress__bar js-posts-slider__progress-bar"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>