<div class="o-join">
    <div class="grid-container grid grid-x height-100 align-middle align-justify">
        <div class="cell small-12 medium-4">
            <div class="o-join__copy">
                <?php the_field('copy') ?>
            </div>
        </div>

        <div class="cell small-12 medium-shrink">
            <div class="o-join__button-container">
                <?php 
                $link = get_field('link');

                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a class="c-button c-button--alpha" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>