<div class="o-members-archive s-members-archive">
    <div class="grid-container">
        <div class="grid grid-x">
            <div class="cell small-12">
                <?php $postTypes = [
                    'webinars' => 'Webinars',
                    'case-studies' => 'Case Studies',
                    'insight-and-research' => 'Insight and<br>Research Reports',
                ]; ?>

                <div class="o-members-archive__nav">
                    <div class="grid-container full">
                        <div class="grid grid-x align-center align-middle tabs" data-tabs data-deep-link="true" id="cpt-tabs">
                            <?php foreach ($postTypes as $postType => $title): ?>
                            <div class="cell small-4 medium-shrink tabs-title <?= $postType == 'webinars' ? 'is-active' : '' ?>">
                                <a class="c-tab" href="#<?= $postType ?>" <?= $postType == 'webinars' ? 'aria-selected="true"' : '' ?>>
                                    <?= $title ?>
                                </a>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <div class="tabs-content" data-tabs-content="cpt-tabs">
                    <?php foreach ($postTypes as $postType => $title): ?>
                    <div class="tabs-panel <?= $postType == 'webinars' ? 'is-active' : '' ?>" id="<?= $postType ?>">

                        <?php
                            $postArguments = [
                                'post_type' => $postType,
                                'posts_per_page' => -1
                            ];

                            switch($postType):
                                case 'webinars':
                                    $postArguments = array_merge($postArguments, [
                                        'meta_key' => 'video'
                                    ]);
                                break;

                                case 'insight-and-research-reports':
                                    $postArguments = array_merge($postArguments, [
                                        'meta_key' => 'download'
                                    ]);
                                break;
                            endswitch;

                            $posts = new WP_Query($postArguments);

                            $rowIndex = 0;
                        ?>

                        <div class="o-members-archive__content o-members-archive__content--<?= $postType ?>">
                            <?php if ($posts->have_posts()): ?>
                                <div class="grid-container full">
                                    <div class="grid grid-x ">
                                        <div class="cell small-12 medium-12">
                                            <div class="o-members-archive__grid">
                                                <?php while($posts->have_posts()): $posts->the_post(); ?>
                                                    <div class="o-members-archive__post-card">
                                                        <?php
                                                        $postId = get_the_ID($post);

                                                        $isPublic = get_field('available_logged_out') || is_user_logged_in();
                                                        $partialPath = 'partials/components/post-card-' . sanitize_title(get_post_type($postId)). '.php';

                                                        if (locate_template($partialPath)):
                                                            include(locate_template($partialPath));
                                                        endif;
                                                        ?>
                                                    </div>
                                                <?php endwhile; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>

                <?php unset($rowIndex); ?>
            </div>
        </div>
    </div>
</div>
