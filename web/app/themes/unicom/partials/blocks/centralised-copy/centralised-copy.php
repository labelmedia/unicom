<div class="c-centralised-copy">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="small-12 cell">
                <div class="c-centralised-copy__content">
                    <?php the_field('copy'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
