<?php if (have_rows('content')): ?>
<div class="o-copy utl-swoop-arrow">
    <div class="grid-container grid grid-x">
        <div class="cell small-12">
            <?php while (have_rows('content')): the_row();  ?>
                <div class="o-copy__row">
                    <div class="grid-container full">
                        <div class="grid grid-x <?= (get_row_index() % 2 != 0) ? 'align-right' : '' ?> align-middle">
                            <div class="cell small-12 medium-6">
                                <h2 class="o-copy__heading">
                                    <?php the_sub_field('heading') ?>
                                </h2>

                                <div class="o-copy__content small-text-center">
                                    <?php the_sub_field('copy') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>