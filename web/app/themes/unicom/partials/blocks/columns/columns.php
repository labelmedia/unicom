
<div class="o-columns">
    <div class="o-columns__inner height-100">
        <div class="grid-container">
            <div class="grid grid-x">
                <div class="cell small-12 medium-8">
                    <div class="o-columns__heading">
                        <?php the_field('copy') ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if (have_rows('columns')): ?>
        <div class="grid-container">
            <div class="grid grid-x">
                <?php while (have_rows('columns')): the_row(); ?>
                    <div class="cell small-12 medium-auto">
                        <div class="o-columns__col <?php echo get_sub_field('hide_copy_mobile') ? 'has-hidden-mob-elems' : ''; ?>">
                            <?php $image = get_sub_field('image'); ?>

                            <?php if( !empty( $image ) ): ?>
                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="o-columns__image" />
                            <?php endif; ?>

                            <div class="o-columns__copy">
                                <?php the_sub_field('copy') ?>
                            </div>

                            <?php
                            $link = get_sub_field('link');
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>

                            <a href="<?= esc_url($link['url']) ?>" target="<?php echo esc_attr( $link_target ); ?>" class="o-columns__link">
                                <?= esc_html($link['title']) ?>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
