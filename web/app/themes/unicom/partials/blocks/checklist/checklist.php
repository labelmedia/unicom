<div class="o-checklist">
    <div class="grid-container">
        <div class="grid grid-x align-justify align-middle o-checklist__inner">
            <div class="cell small-12 medium-5">
                <h3 class="o-checklist__heading">
                    <?php the_field('heading') ?>
                </h3>

                <?php if( have_rows('checklist') ): ?>
                    <ul class="c-checklist">
                        <?php while ( have_rows('checklist') ) : the_row(); ?>
                            <li class="c-checklist__li">
                                <span>
                                    <?php the_sub_field('text') ?>
                                </span>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>

                <?php $link = get_field('link'); ?>

                <?php if( $link ): ?>
                    <?php
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    
                    <a class="c-button c-button--alpha" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <?php echo esc_html( $link_title ); ?>
                    </a>
                <?php endif; ?>
            </div>

            <div class="cell small-12 medium-5 js-cards-slider o-checklist__slider">
                <div class="o-checklist__main-webinar-preview o-checklist__webinar-preview--main">
                    <?php if ($postId = get_field('webinar')): ?>
                        <?php $isPublic = get_field('available_logged_out') || is_user_logged_in(); ?>
                        <?php include(locate_template('partials/components/post-card-webinars.php')) ?>
                    <?php endif ; ?>
                </div>
        
                <div class="o-checklist__webinar-preview o-checklist__webinar-preview--top-right">
                    <?php $acfField = 'image_one' ?>
                    <?php include(locate_template('partials/components/webinar-preview.php')) ?>
                </div>

                <div class="o-checklist__webinar-preview o-checklist__webinar-preview--bottom-right">
                    <?php $acfField = 'image_two' ?>
                    <?php include(locate_template('partials/components/webinar-preview.php')) ?>
                </div>

                <div class="o-checklist__webinar-preview o-checklist__webinar-preview--bottom">
                    <?php $acfField = 'image_three' ?>
                    <?php include(locate_template('partials/components/webinar-preview.php')) ?>
                </div>
            </div>
        </div>
    </div>
</div>