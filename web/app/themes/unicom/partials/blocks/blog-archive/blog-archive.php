<div class="o-blog-archive">
    <div class="grid-container">
        <div class="grid grid-x">
            <div class="cell small-12">
                <?php
                    $postArguments = [
                        'post_type' => 'post',
                        'posts_per_page' => -1
                    ];

                    $posts = new WP_Query($postArguments);

                    $rowIndex = 0;
                ?>

                <div class="o-blog-archive__content o-blog-archive__content--posts">
                    <?php if ($posts->have_posts()): ?>
                        <div class="grid-container full">
                            <div class="grid grid-x ">
                                <div class="cell small-12 medium-12">
                                    <div class="o-blog-archive__grid">
                                        <?php while($posts->have_posts()): $posts->the_post(); ?>
                                            <div class="o-blog-archive__post-card">
                                                <?php
                                                $postId = get_the_ID($post);
                                                
                                                $partialPath = 'partials/components/post-card-post.php';

                                                if (locate_template($partialPath)):
                                                    include(locate_template($partialPath));
                                                endif;
                                                ?>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>     
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>