<div class="o-gallery-slider js-gallery-slider">
    <?php if (get_field('heading')): ?>
        <div class="grid-container">
            <div class="grid grid-x">
                <div class="small-12 text-center">
                    <div class="o-gallery-slider__heading utl-color-beta">
                        <?php the_field('heading'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="o-gallery-slider__grid-container">
        <div class="grid grid-x">
            <div class="cell small-12">
                <div class="o-gallery-slider__container">
                    <?php if( have_rows('gallery_slider') ): ?>
                        <div class="c-gallery-slider s-gallery-slider js-gallery-slider__slide swiper-container" data-count="<?= count(get_field('gallery_slider')) ?>">
                            <div class="swiper-wrapper">
                                <?php while ( have_rows('gallery_slider') ) : the_row(); ?>
                                    <div class="swiper-slide">
                                        <div class="c-gallery-slider__slide">
                                            <div class="c-gallery-slider__slide-inner">
                                                <?php if (have_rows('background_image')): while (have_rows('background_image')): the_row(); ?>
                                                    <?php $image = get_sub_field('desktop'); ?> 
                                                    
                                                    <?php if( !empty( $image ) ): ?>
                                                        <div class="c-gallery-slider__cover c-gallery-slider__background show-for-medium" style="background-image:url(<?= esc_url($image['url']) ?>)"></div>
                                                    <?php endif; ?>

                                                    <?php $image = get_sub_field('mobile'); ?>

                                                    <?php if( !empty( $image ) ): ?>
                                                        <div class="c-gallery-slider__cover c-gallery-slider__background show-for-small-only" style="background-image:url(<?= esc_url($image['url']) ?>)"></div>
                                                    <?php endif; ?>

                                                    <?php unset($image); ?>
                                                <?php endwhile; endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="o-widget-container o-widget-container--gallery-slider grid-container js-widget-container">
        <div class="grid grid-x align-center">
            <div class="cell small-12">
                <div class="grid-container full">
                    <div class="grid grid-x align-right">
                        <div class="cell small-12 medium-8 large-6">
                            <div class="c-widget c-widget--gallery-slider c-widget--circle js-widget">
                                <?php while ( have_rows('gallery_slider') ) : the_row(); ?>
                                    <div class="c-widget__inner js-gallery-slider__widget <?= get_row_index() !== 1 ? 'is-hidden' : '' ?>" data-index="<?= get_row_index() ?>">
                                        <div class="c-widget__cover"></div>

                                        <div class="c-widget__title">
                                            <?php the_sub_field('heading') ?>
                                        </div>

                                        <div class="c-widget__copy">
                                            <?php the_sub_field('content') ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid grid-x align-center">
            <div class="cell small-12 large-10">
                <div class="c-slider-progress">
                    <div class="c-slider-progress__cta">
                        <a href="#" class="c-slider-progress__slider-nav c-slider-progress__slider-nav--previous js-gallery-slider__prev">
                            Previous
                        </a>

                        <a href="#" class="c-slider-progress__slider-nav c-slider-progress__slider-nav--next js-gallery-slider__next">
                            Next
                        </a>
                    </div>

                    <div class="c-slider-progress__bar-container">
                        <div class="c-slider-progress__bar-full">
                            <span class="c-slider-progress__bar js-gallery-slider__progress-bar"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>