<?php if (have_rows('content')): ?>
<div class="o-image-copy">
    <div class="o-image-copy__inner">
        <div class="grid-container full">
            <div class="grid grid-x">
                <div class="cell small-12">
                    <?php while (have_rows('content')): the_row();  ?>
                        <div class="o-image-copy__row">
                            <div class="grid-container full">
                                <div class="grid grid-x <?= (get_row_index() % 2 == 0) ? 'align-right' : '' ?> align-middle">
                                    <div class="cell small-12 medium-8">
                                        <?php $image = get_sub_field('image'); ?>

                                        <?php
                                        if( !empty( $image ) ): ?>
                                            <img src="<?= esc_url($image['url']); ?>" alt="<?= esc_attr($image['alt']); ?>" />
                                        <?php endif; ?>
                                    </div>

                                    <div class="o-image-copy__widget-container">
                                        <div class="grid-container full">
                                            <div class="grid grid-x <?= (get_row_index() % 2 != 0) ? 'align-right' : '' ?>">
                                                <div class="cell small-12 medium-8 large-6">
                                                    <div class="c-widget c-widget--gallery-slider <?= (get_row_index() % 2 != 0) ? 'c-widget--arrow' : '' ?>">
                                                        <div class="c-widget__inner">
                                                            <div class="c-widget__cover"></div>
                                                            
                                                            <h2 class="c-widget__title">
                                                                <?php the_sub_field('heading') ?>
                                                            </h2>

                                                            <div class="c-widget__content">
                                                                <?php the_sub_field('content') ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>