<div class="o-insights" id="find-out-more">
    <div class="grid-container">
        <div class="grid grid-x align-justify align-middle">
            <div class="cell small-12 medium-6">
                <h3>
                    <?php the_field('heading') ?>
                </h3>
            </div>

            <div class="cell small-12 medium-shrink">
                <?php if( have_rows('checklist') ): ?>
                    <ul class="c-checklist">
                        <?php while ( have_rows('checklist') ) : the_row(); ?>
                            <li class="c-checklist__li">
                                <span>
                                    <?php the_sub_field('text') ?>
                                </span>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>