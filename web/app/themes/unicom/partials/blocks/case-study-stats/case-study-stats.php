<?php if (have_rows('stats')): ?>
<div class="o-case-study-stats">
    <div class="grid-container">
        <div class="grid grid-x align-justify">
            <?php while (have_rows('stats')): the_row() ?>
            <div class="cell small-12 medium-shrink text-center">
                <div class="c-stat">
                    <div class="c-stat__heading">
                        <?php the_sub_field('number') ?>
                    </div>

                    <div class="c-stat__description">
                        <?php the_sub_field('description') ?>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>