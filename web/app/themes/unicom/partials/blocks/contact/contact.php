<div class="o-contact">
    <div class="grid-container">
        <div class="grid grid-x">
            <div class="cell small-12">
                <div class="o-contact__heading">
                    <?php the_field('global_contact_heading', 'options') ?>
                </div>
            </div>

            <div class="cell small-12 medium-4">
                <div class="o-contact__dropdown-form s-contact-preview-dropdown s-dropkick js-gravity-form">
                    <?= do_shortcode('[gravityform id="' . get_field('global_gravity_id', 'options') . '" title="false" description="false" ajax="true"]') ?>
                </div>
            </div>
        </div>
    </div>
</div>