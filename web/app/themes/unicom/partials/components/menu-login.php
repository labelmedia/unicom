<div class="c-menu c-menu--login-register js-menu s-gravity-form s-login-form <?= (isset($_GET['menu']) && $_GET['menu'] == 'login') ? 'is-active' : '' ?>" data-menu="login">
    <a href="#" class="c-menu__close js-menu__close"></a>

    <div class="c-menu__content">
        <div class="c-menu__heading">
            <span>Unicom Insights.</span><br class="hide-for-small-only">
            Log in
        </div>

        <div class="js-menu__login-form">
            <?= do_shortcode('[gravityform action="login" logged_in_message="You are logged in. Click <a href="' . home_url('members') . '">here</a> to visit the Members Area." title="false" description="false" ajax="true"]') ?>
        </div>

        <div class="grid-container full text-center">
            <div class="grid grid-x align-center">
                <div class="cell shrink">
                    <a class="c-menu__small-link c-menu__small-link--forgot" href="<?= esc_url(wp_lostpassword_url(get_home_url())); ?>">
                        Forgot Password?
                    </a>
                </div>

                <div class="cell shrink">
                    <a data-target="register" class="js-toggle-nav c-menu__small-link" href="<?php global $wp; echo home_url($wp->request . '?menu=register') ?>">
                        Don't have an account? Register
                    </a>
                </div>
            </div>
        </div>

        <div class="js-menu__gravity-form-footer grid-container full text-left">
            <div class="grid grid-x align-middle align-justify">
                <div class="cell small-12 medium-6">
                    <a href="#" class="c-button c-button--block c-button--beta js-menu__submit" data-form=".js-menu__login-form">
                        Log In
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>