
<div class="c-webinar-preview <?= isset($overlay) ? 'c-webinar-preview--overlay' : '' ?>" style="background-image:url('<?php the_field($acfField) ?>')">
    <div class="c-webinar-preview__inner"></div>
</div>

<?php unset($overlay) ?>