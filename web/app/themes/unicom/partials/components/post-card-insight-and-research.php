<?php $landing_page = get_field('report_landing_page', $postId); ?>
<?php if(isset($landing_page['url'])): ?>
    <a class="c-post-card is-dark" href="<?= $landing_page['url']; ?>">
        <div class="c-post-card__background-colour"></div>

        <div class="c-post-card__inner">
            <h4 class="c-post-card__heading">
                <?= get_the_title($postId) ?>
            </h4>

            <div class="c-post-card__taxonomy">
                RESEARCH REPORT
            </div>
        </div>
    </a>
<?php else: ?>
    <div class="c-post-card is-dark">
        <div class="c-post-card__background-colour"></div>

        <div class="c-post-card__inner">
            <h4 class="c-post-card__heading">
                <?= get_the_title($postId) ?>
            </h4>

            <div class="c-post-card__taxonomy">
                RESEARCH REPORT
            </div>
        </div>
    </div>
<?php endif; ?>
