<div class="c-menu c-menu--login-register js-menu s-gravity-form s-register-form <?= (isset($_GET['menu']) && $_GET['menu'] == 'register') ? 'is-active' : '' ?>" data-menu="register">
    <a href="#" class="c-menu__close js-menu__close"></a>

    <div class="c-menu__content">
        <div class="c-menu__heading">
            <span>Unicom Insights.<br class="hide-for-small-only"></span>
            Sign up and never<br class="hide-for-small-only">
            look back
        </div>

        <div class="js-menu__register-form">
            <?= do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]') ?>
        </div>

        <div class="js-menu__gravity-form-footer grid-container full text-left">
            <div class="grid grid-x align-middle align-justify">
                <div class="cell small-12 medium-6">
                    <a href="#" class="c-button c-button--block c-button--beta js-menu__submit" data-form=".js-menu__register-form">
                        Join Unicoms Insights
                    </a>
                </div>

                <div class="cell shrink">
                    <div class="c-menu__register-notice">
                        <p>
                            <a href="<?php global $wp; echo home_url($wp->request . '?menu=login') ?>" class="js-toggle-nav">
                                Already Registered? - Log In
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>