<?php
$nowDateTimeObject = new DateTime('+0000');
$nowDateTimeString = $nowDateTimeObject->format('Y-m-d H:i:s');

$args = array(
    'post_type' => 'webinars',
    'posts_per_page' => 1,
    'orderby'=> [
        'webinar_datetime' => 'ASC'
    ],
    'meta_query' => array(
        'relation' => 'AND',
        'webinar_datetime' => array(
            'key' => 'webinar_datetime'
        ),
        'date_range' => array(
            'key' => 'webinar_datetime',
            'compare' => '>',
            'value' => $nowDateTimeString
        )
    )
);

if ($webinarId = get_field('webinar_widget_override', 'option')):
    $args = array_merge($args, array(
        'p' => $webinarId
    ));
endif;

$query = new WP_Query($args);
?>

<?php if ($query->have_posts()): ?>
    <?php while ($query->have_posts()): $query->the_post(); ?>
        <?php
        $postDateTimeString = get_field('webinar_datetime', get_the_ID());
        $postDateTimeObject = new DateTime($postDateTimeString);
        $dateDifference = date_diff($nowDateTimeObject, $postDateTimeObject);
        ?>

        <div class="o-widget-container o-widget-container--hero grid-container js-widget-container">
            <div class="grid grid-x align-center">
                <div class="cell small-12">
                    <div class="grid-container full">
                        <div class="grid grid-x align-right">
                            <div class="cell small-12 medium-5">
                                <div class="c-widget c-widget--hero text-center js-hero__widget js-widget">
                                    <div class="c-widget__inner">
                                        <div class="c-widget__cover"></div>

                                        <div class="c-widget__content">
                                            <div class="c-widget__small-title">
                                                NEXT WEBINAR
                                            </div>

                                            <div class="c-widget__date-stamp">
                                                <?= strtoupper($postDateTimeObject->format('dS F ga')) ?>
                                            </div>

                                            <div class="c-widget__countdown">
                                                <div class="grid-container full">
                                                    <div class="grid grid-x align-center">
                                                        <div class="cell shrink">
                                                            <div class="c-webinar__countdown-col c-webinar__countdown-col--border">
                                                                <div class="c-widget__countdown-header">
                                                                    <?= $dateDifference->d <= 9 ? '0' . $dateDifference->d : $dateDifference->d ?>
                                                                </div>

                                                                <div class="c-widget__countdown-unit">
                                                                    D
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="cell shrink">
                                                            <div class="c-webinar__countdown-col c-webinar__countdown-col--border">
                                                                <div class="c-widget__countdown-header">
                                                                    <?= $dateDifference->h <= 9 ? '0' . $dateDifference->h : $dateDifference->h ?>
                                                                </div>

                                                                <div class="c-widget__countdown-unit">
                                                                    H
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="cell shrink">
                                                            <div class="c-webinar__countdown-col">
                                                                <div class="c-widget__countdown-header">
                                                                    <?= $dateDifference->i <= 9 ? '0' . $dateDifference->i : $dateDifference->i ?>
                                                                </div>

                                                                <div class="c-widget__countdown-unit">
                                                                    M
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="c-widget__title">
                                                <?= get_the_title(get_the_ID()); ?>
                                            </div>

                                            <?php if ($link = get_field('link', get_the_ID())): ?>
                                            <div class="c-webinar-widet__cta">
                                                <a href="<?= $link ?>" class="c-button c-button--alpha">
                                                    Register
                                                </a>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
