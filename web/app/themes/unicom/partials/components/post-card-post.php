<a class="c-post-card c-post-card--unlocked" href="<?php the_permalink() ?>">
    <div class="c-post-card__background-image" style="background-image: url('<?php the_field('preview_image', $postId) ?>')"style="background-image: url('<?php the_field('preview_image', $postId) ?>')"></div>

    <div class="c-post-card__inner">
        <h4 class="c-post-card__heading">
            <?= get_the_title($postId) ?>
        </h4>

        <div class="c-post-card__taxonomy">
            BLOG
        </div>
    </div>
</a>