<a class="c-post-card <?= $isPublic ? 'c-post-card--unlocked' : 'c-post-card--locked js-locked' ?>" href="<?= $isPublic ? the_permalink($postId) : home_url('/?menu=register') ?>">
    <div class="c-post-card__background-image" style="background-image: url('<?php the_field('preview_image', $postId) ?>')"style="background-image: url('<?php the_field('preview_image', $postId) ?>')"></div>

    <div class="c-post-card__inner">
        <h4 class="c-post-card__heading">
            <?= get_the_title($postId) ?>
        </h4>

        <div class="c-post-card__taxonomy">
            CASE STUDY
        </div>

        <?php if (!$isPublic): ?>
            <span class="c-post-card__locked-label">
                JOIN TO VIEW
            </span>
        <?php endif; ?>
    </div>
</a>