<a class="c-post-card <?= $isPublic ? 'c-post-card--unlocked' : 'c-post-card--locked js-locked' ?>" href="<?= $isPublic ? get_field('link', $postId) : home_url('/?menu=register') ?>" <?= $isPublic ? "data-open='webinar-video-$postId' onclick='return false'" : '' ?>>
    <div class="c-post-card__background-image" style="background-image: url('<?php the_field('preview_image', $postId) ?>')"style="background-image: url('<?php the_field('preview_image', $postId) ?>')"></div>

    <div class="c-post-card__inner">
        <h4 class="c-post-card__heading">
            <?= get_the_title($postId) ?>
        </h4>

        <div class="c-post-card__taxonomy">
            WEBINAR
        </div>

        <?php if ($isPublic): ?>
            <?php if (get_field('minutes', $postId)): ?>
                <div class="c-post-card__meta">
                    <?php the_field('minutes', $postId) ?> MINUTES
                </div>
            <?php endif; ?>
        <?php else: ?>
            <span class="c-post-card__locked-label">
                JOIN TO VIEW
            </span>
        <?php endif; ?>
    </div>
</a>

<?php $video = get_field('video', $postId) ?>

<?php if ($video && $isPublic): ?>
    <div class="c-video-modal s-video-modal reveal" id="webinar-video-<?= $postId ?>" data-reveal>
        <div class="c-video-modal__container">
            <?= $video ?>
        </div>
    </div>
<?php endif; ?>
