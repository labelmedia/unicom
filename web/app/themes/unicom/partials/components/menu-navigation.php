<div class="c-menu c-menu--navigation js-menu <?= (isset($_GET['menu']) && $_GET['menu'] == 'navigation') ? 'is-active' : '' ?>" data-menu="navigation">
    <a href="#" class="c-menu__close js-menu__close"></a>

    <div class="c-menu__content">
        <?php if( have_rows('global_navigation','option')): ?>
            <nav class="c-menu__nav">
                <ul class="c-menu__list-container">
                    <?php while ( have_rows('global_navigation','option') ) : the_row(); ?>

                        <?php if (get_sub_field('logged_in_only') && is_user_logged_in() || !get_sub_field('logged_in_only')): ?>

                        <li class="c-menu__list-item">
                            <?php 
                            $link = get_sub_field('page_link');
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                    
                            <a href="<?= esc_url($link['url']) ?>" target="<?php echo esc_attr( $link_target ); ?>" class="c-menu__link">
                                <?= esc_html($link['title']) ?>
                            </a>
                        </li>

                        <?php endif; ?>

                    <?php endwhile; ?>
                </ul>
            </nav>
        <?php endif; ?>

        <div class="c-menu__note">
            We’re experts in targeting, talking and listening<br>
            to students in the most compelling way possible.
        </div>
    </div>
</div>
