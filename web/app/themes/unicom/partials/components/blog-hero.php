<div class="o-hero-slider js-hero" data-aos-duration="300" data-aos="fade-up">
    <div class="grid-container full grid grid-x">
        <div class="cell small-12">
            <div class="o-hero-slider__container">
                <div class="c-hero-slide">
                    <div class="c-hero-slide__slide">
                        <?php if (have_rows('background_image')): while (have_rows('background_image')): the_row(); ?>
                            <?php $image = get_sub_field('desktop'); ?> 
                            
                            <?php if( !empty( $image ) ): ?>
                                <div class="c-hero-slide__cover c-hero-slide__background show-for-medium" style="background-image:url(<?= esc_url($image['url']) ?>)"></div>
                            <?php endif; ?>

                            <?php $image = get_sub_field('mobile'); ?>

                            <?php if( !empty( $image ) ): ?>
                                <div class="c-hero-slide__cover c-hero-slide__background show-for-small-only" style="background-image:url(<?= esc_url($image['url']) ?>)"></div>
                            <?php endif; ?>

                            <?php unset($image); ?>
                        <?php endwhile; endif; ?>

                        <div class="c-hero-slide__cover o-hero-slide__content">
                            <div class="grid-container full height-100">
                                <div class="grid grid-x height-100 align-middle align-center">
                                    <div class="cell small-11">
                                        <div class="c-hero-slide__sub-heading utl-color-beta">
                                            BLOG
                                        </div>

                                        <div class="c-hero-slide__heading">
                                            <?php the_title() ?>
                                        </div>

                                        <?php 
                                        $link = get_sub_field('cta');
                                        
                                        if( $link ): 
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                        ?>
                                        
                                        <a href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>" class="c-button c-button--alpha">
                                            <?= esc_html( $link_title ); ?>
                                        </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (get_field('has_arrow')): ?>
        <?php get_template_part('partials/components/hero-arrow') ?>
    <?php endif; ?>
</div>

<?php if (get_field('has_widget')): ?>
    <?php get_template_part('partials/components/widget-hero') ?>
<?php endif; ?>