<?php get_header(); ?>
    <?php get_template_part('partials/blocks/hero-slider/hero-slider') ?>
    <?php get_template_part('partials/blocks/case-study-stats/case-study-stats') ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="case-study">
            <div class="grid-container">
                <div class="grid grid-x align-center">
                    <div class="cell small-12 medium-8">
                        <div class="case-study__inner s-wysiwyg">
                            <?php the_content() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>

    <?php get_template_part('partials/blocks/related-posts/related-posts') ?>
    <?php get_template_part('partials/blocks/contact/contact') ?>
<?php get_footer(); ?>