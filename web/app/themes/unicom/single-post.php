<?php get_header(); ?>
    <?php get_template_part('partials/components/blog-hero') ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="blog-post">
            <div class="grid-container">
                <div class="grid grid-x align-center">
                    <div class="cell small-12 medium-8">
                        <div class="blog-post__meta">
                            <?php if ($author = get_field('author')): ?>
                                <span class="blog-post__author">
                                    <?= $author ?>
                                </span>
                            <?php endif; ?>

                            <?php if ($date = get_the_date('dS F Y')): ?>
                                <span class="blog-post__date">
                                    <?= $date ?>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="blog-post__inner s-wysiwyg">
                            <?php the_content() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>

    <?php get_template_part('partials/blocks/contact/contact') ?>
<?php get_footer(); ?>