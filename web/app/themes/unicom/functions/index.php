<?php
require_once __DIR__.'/acf/index.php';
require_once __DIR__.'/admin.php';
require_once __DIR__.'/cpt-tax.php';
require_once __DIR__.'/search.php';
require_once __DIR__.'/theme.php';
require_once __DIR__.'/users.php';
require_once __DIR__.'/scripts.php';
