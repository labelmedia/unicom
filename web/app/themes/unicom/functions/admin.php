<?php

/**
 * Login overrides
 */

// Replace WordPress logo on login page
add_action('login_enqueue_scripts', function() { ?>
    <style>
        #login h1 a {
            width: auto;
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logo.png');
            background-size: contain;
            background-position: center;
        }
    </style>
<?php });

// Change WordPress logo link to home page
add_filter('login_headerurl', function() {
    return home_url();
});

// Change logo link title to site title
add_filter('login_headertext', function() {
    return get_bloginfo();
});

// Custom redirects when logging in
// add_filter('login_redirect', 'custom_wp_admin_redirects', 10, 3);
// function custom_wp_admin_redirects($redirect_to, $request, $user) {
//     global $user;

//     if(isset($user->roles) && is_array($user->roles)) {
//         if(in_array('administrator', $user->roles)) {
//             return $redirect_to;
//         }
//         else {
//             return home_url();
//         }
//     }
//     else {
//         return $redirect_to;
//     }
// }

// Remove login box shake
add_action('login_head', function() {
    remove_action('login_head', 'wp_shake_js', 12);
});

// Remove the 'Thank you' from the admin footer
add_filter('admin_footer_text', '__return_empty_string');

// Edit layout of user pages
remove_action('admin_color_scheme_picker', 'admin_color_scheme_picker');
add_action('admin_head', 'profile_subject_start');
add_action('admin_footer', 'profile_subject_end');
if(!function_exists('profile_remove_personal_options')) {
    // Removes the leftover 'Visual Editor', 'Keyboard Shortcuts' and 'Toolbar' options.
    function profile_remove_personal_options($subject) {
        $subject = preg_replace('#<h2>Personal Options</h2>.+?/table>#s', '', $subject, 1);
        $subject = preg_replace('#<h2 id="wordpress-seo">Yoast SEO settings</h2>.+?/table>#s', '', $subject, 1);
        $subject = preg_replace('#<h3>Additional Capabilities</h3>.+?/table>#s', '', $subject, 1);
        return $subject;
    }

    function profile_subject_start() {
        ob_start('profile_remove_personal_options');
    }

    function profile_subject_end() {
        ob_end_flush();
    }
}

// Custom Admin styles
function custom_wp_admin_css() {
    $filePath = glob(get_template_directory() . '/dist/css/admin{*}.css', GLOB_BRACE);
    $filename = basename($filePath[0]);

    wp_enqueue_style('admin_css', get_stylesheet_directory_uri() . "/dist/css/$filename");
}

add_action('admin_print_styles', 'custom_wp_admin_css');
add_action('admin_enqueue_scripts', 'custom_wp_admin_css');
add_action('login_enqueue_scripts', 'custom_wp_admin_css');

// Disable default Dashboard widgets
add_action('admin_init', function() {
    // remove_meta_box('dashboard_right_now', 'dashboard', 'core');        // Right Now Widget
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'core');  // Comments Widget
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');   // Incoming Links Widget
    remove_meta_box('dashboard_plugins', 'dashboard', 'core');          // Plugins Widget
    remove_meta_box('dashboard_quick_press', 'dashboard', 'core');      // Quick Press Widget
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');    // Recent Drafts Widget
    remove_meta_box('dashboard_primary', 'dashboard', 'core');          //
    remove_meta_box('dashboard_secondary', 'dashboard', 'core');        //
    remove_meta_box('yoast_db_widget', 'dashboard', 'normal');          // Yoast's SEO Plugin Widget
});

// Disable 'Additional CSS' panel from Customiser
add_action('customize_register', function($wp_customize) {
    $wp_customize->remove_section('custom_css');
}, 15);

// Move Yoast SEO box to bottom of post/page
add_filter('wpseo_metabox_prio', function() {
    return 'low';
});

// Show environment notification on staging login screen
add_filter('login_message', function() {
    if(getenv('WP_ENV') != 'staging') {
        return false;
    } ?>

    <div style="margin-bottom: 10px; padding: 10px; border: solid 1px #ebccd1; font-size: 14px; text-align: center; background-color: #f2dede;">
        <p>It looks like you're on the <strong>STAGING</strong> site.</p>

        <?php if($production_domain = getenv('PRODUCTION_DOMAIN')): ?>
            <a style="display: block; margin-top: 10px;" href="<?php echo "$production_domain{$_SERVER['REQUEST_URI']}" ?>">
                Click here to update content on your live site.
            </a>
        <?php endif; ?>
    </div>
<?php });

// Show environment notification on staging Admin Bar
add_action('wp_before_admin_bar_render', function() {
    global $wp_admin_bar;

    if(getenv('WP_ENV') != 'staging') {
        return false;
    }

    $env_notification_link = '';

    if($admin_url = getenv('PRODUCTION_DOMAIN')) {
        $env_notification_link = <<<HTML
            <a style="display: inline-block; padding: 0; color: #fff; font-weight: bold; text-decoration: underline;" href="$admin_url{$_SERVER['REQUEST_URI']}">
                Click here to update content on your live site.
            </a>
HTML;
    }

    $env_notification = <<<HTML
        <div style="height: 32px; padding: 0 10px; color: #fff; text-align: center; background-color: #d54e21;">
            It looks like you're on the <strong style="font-weight: bold;">STAGING</strong> site.
            $env_notification_link
        </div>
HTML;

    $wp_admin_bar->add_node([
        'id'    => 'env_notification',
        'title' => $env_notification
    ]);
}, 999);

// add shortcode
function button_function( $atts = array(), $content = null ) {

    // set up default parameters
    extract(shortcode_atts(array(
     'link' => '#'
    ), $atts));

    return '<a href="'. $link .'" target="blank" class="c-button c-button--alpha">' . $content . '</a>';
}

add_shortcode('button', 'button_function');
