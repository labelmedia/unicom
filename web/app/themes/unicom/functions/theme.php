<?php

// Initial theme config
add_action('after_setup_theme', function() {
    add_theme_support('automatic-feed-links');
    add_theme_support('html5', [
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption'
    ]);
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support( 'woocommerce' );

    register_nav_menus([
        'primary' => 'Primary Menu',
    ]);
});

// Enqueue scripts and styles
function enqueue_theme_styles() {
    // Enqueue every css and js files in theme assets folders
    $filePaths = glob(get_template_directory() . '/dist/{css,js}/*.{css,js}', GLOB_BRACE);

    foreach ($filePaths as $filePath) {
        $filename = basename($filePath);
        $label = substr($filename, 0, strpos($filename, '.'));
        $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);

        if ($fileExtension == 'css') {
            wp_enqueue_style($label, get_stylesheet_directory_uri() . "/dist/css/$filename");
        } elseif ($fileExtension == 'js') {
            wp_enqueue_script($label, get_stylesheet_directory_uri() . "/dist/js/$filename", ['gform_gravityforms'], null, true);
        }
    }
}

add_action('wp_enqueue_scripts', 'enqueue_theme_styles');

// Add page slug to body classes
add_filter('body_class', function($classes) {
    global $post;

    if(isset($post)) {
        $classes[] = 'page-'.$post->post_name;
    }

    return $classes;
});

// Redirect attachment pages to home or to parent page if available
add_action('template_redirect', function() {
    if(is_attachment()) {
        global $post;

        if($post && $post->post_parent) {
            $redirectUrl = esc_url(get_permalink($post->post_parent));
        }
        else {
            $redirectUrl = esc_url(home_url('/'));
        }

        wp_redirect($redirectUrl, 301);
        exit;
    }
});

// return fullscreen vimeo
function returnOembed($iframe) {

    if ($iframe):

        // Use preg_match to find iframe src.
        preg_match('/src="(.+?)"/', $iframe, $matches);
        $src = $matches[1];

        // Add extra parameters to src and replcae HTML.
        $params = array(
            'controls'  => false,
            'autoplay' => true,
            'background' => true
        );

        $new_src = add_query_arg($params, $src);
        $iframe = str_replace($src, $new_src, $iframe);

        // Add extra attributes to iframe HTML.
        $attributes = 'frameborder="0"';
        $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

        // Display customized HTML.
        return $iframe;

    endif;
}

// allow HTML email through wp_mail()

function set_content_type(){
    return "text/html";
}

add_filter( 'wp_mail_content_type','set_content_type' );

// allow svg
function svg_support($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'svg_support');

function register_posttypes() {
    register_post_type( 'webinars',
        array(
            'labels' => array(
                'name' => __( 'Webinars' ),
                'singular_name' => __( 'Webinar' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'webinar'),
            'supports' => array( 'title', 'thumbnail'),
            'show_in_rest' => true,
            'taxonomies' => array('webinar-category'),
            'publicly_queryable'  => false
        )
    );

    register_post_type( 'case-studies',
        array(
            'labels' => array(
                'name' => __( 'Case Studies' ),
                'singular_name' => __( 'Case Study' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'case-study'),
            'supports' => array( 'title', 'thumbnail', 'editor'),
            'show_in_rest' => true,
        )
    );

    register_post_type( 'insight-and-research',
        array(
            'labels' => array(
                'name' => __( 'Insights & Research Reports' ),
                'singular_name' => __( 'Report' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'reports'),
            'supports' => array( 'title', 'thumbnail'),
            'show_in_rest' => true,
        )
    );
}

add_action('init', 'register_posttypes');

// disable gravity forms page scrolling on submission
add_filter( 'gform_confirmation_anchor', '__return_false' );

// gravity forms JS response
function gravity_forms_submit() {
    echo "<script>
    jQuery(document).on('gform_confirmation_loaded', function() {
        jQuery('.js-menu__gravity-form-footer').addClass('is-hidden');
    });

    jQuery(window).load(function(){
        window.initDropdowns();

        jQuery(document).on('gform_post_render', function() {
            window.initDropdowns();
        });

        jQuery(document).ajaxSuccess(function(){
            window.initDropdowns()
        });
    });

    </script>";
}

add_action('wp_head', 'gravity_forms_submit', 99);

// populating contact form dropdown dyamically
function populate_dropdown($form) {
    if ($form['id'] != 2) return $form;

    $items = [];
    $dropdownValues = GFAPI::get_field(6, 6);

    foreach($dropdownValues->choices as $choice):
        $items[] = array(
            'text' => $choice['text'],
            'value' => $choice['value'],
        );
    endforeach;


    $form['fields'][0]->choices = $items;

    return $form;
}

add_filter('gform_pre_render','populate_dropdown');

// passing in GET values
function populate_get($form) {
    if ($form['id'] != 6) return $form;

    $items = [];
    $dropdownValues = GFAPI::get_field(6, 6);

    foreach($dropdownValues->choices as $choice):
        $items[] = array(
            'text' => $choice['text'],
            'value' => $choice['value'],
            'isSelected' => isset($_GET['choice']) && $_GET['choice'] == $choice['value'] ? true : false
        );
    endforeach;

    foreach ($form['fields'] as $field):
        if ($field['id'] == 6) {
            $field->choices = $items;
        }
    endforeach;

    return $form;
}

add_filter('gform_pre_render', 'populate_get');

// restricting members access to /members page
add_action('template_redirect', function() {
    global $post;

    $redirect = true;

    if (is_page('members') || is_singular('case-studies')):
        if (is_user_logged_in()):
            $redirect = false;
        endif;

        if ($redirect): 
            wp_redirect(home_url('/?menu=login'));
        endif;
    endif;
});

// redirect login to members page
add_filter( 'gform_user_registration_login_redirect_url', function ( $login_redirect, $sign_on ) {
    return home_url('members');
}, 10, 2 );