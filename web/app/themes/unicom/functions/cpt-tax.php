<?php
/*
    Examples: https://github.com/jjgrainger/PostTypes
    Dash Icons: https://developer.wordpress.org/resource/dashicons/
*/

use PostTypes\PostType;

// Add CPT
// $testimonial = new PostType(
//     [
//         'name'     => 'testimonial',
//         'singular' => 'Testimonial',
//         'plural'   => 'Testimonials',
//         'slug'     => 'testimonials'
//     ],
//     [
//         'show_in_rest' => true,
//         'supports'    => ['title', 'editor', 'thumbnail', 'revisions'],
//         'public'      => true,
//         'has_archive' => false
//     ]
// );
// $testimonial->icon('dashicons-format-quote');
// $testimonial->taxonomy([
//     'name'     => 'testimonial_category',
//     'singular' => 'Testimonial Category',
//     'plural'   => 'Testimonial Categories',
//     'slug'     => 'testimonial_category'
// ]);
// $testimonial->register();
