<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

// Define Global Options
if(function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'Theme Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'global-content',
        'capability' => 'create_users',
        'redirect' => false
    ]);
}

function return_path($path) {
    return 'assets/beautiful-flexible';
}

add_filter( 'bea.beautiful_flexible.images_path', 'return_path');

$fieldGroups = [];

// Global content fields
require_once __DIR__.'/global.php';

// Page specific fields
foreach(glob(__DIR__.'/pages/*.php') as $page) {
    require_once $page;
}

// Post-type specific fields
foreach(glob(__DIR__.'/post-types/*.php') as $post_type) {
    require_once $post_type;
}

// Block specific fields
foreach(glob(__DIR__.'/blocks/*.php') as $block) {
    require_once $block;
}

// Init all fields
add_action('acf/init', function() use ($fieldGroups) {
    foreach($fieldGroups as $fieldGroup) {
        acf_add_local_field_group($fieldGroup->build());
    }
});

// Google API Key
add_action('acf/init', function() {
    acf_update_setting('google_api_key', env('GM_API_KEY'));
});

// Gutenburg blocks

function register_acf_block_types() {
    acf_register_block_type(
        array(
            'name' => 'hero-slider',
            'title' => __('Hero Slider'),
            'description' => __('Slider banner.'),
            'render_template' => 'partials/blocks/hero-slider/hero-slider.php',
        )
    );

    acf_register_block_type(
        array(
            'name' => 'intro',
            'title' => __('Intro'),
            'description' => __('Intro copy.'),
            'render_template' => 'partials/blocks/intro/intro.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'join',
            'title' => __('Join'),
            'description' => __('Join.'),
            'render_template' => 'partials/blocks/join/join.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'copy-block',
            'title' => __('Copy repeater'),
            'description' => __('Alternating copy rows.'),
            'render_template' => 'partials/blocks/copy/copy.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'title',
            'title' => __('Title block'),
            'render_template' => 'partials/blocks/title/title.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'columns',
            'title' => __('Columns'),
            'description' => __('Auto-width columns.'),
            'render_template' => 'partials/blocks/columns/columns.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'contact',
            'title' => __('Contact'),
            'description' => __('Contact block / form.'),
            'render_template' => 'partials/blocks/contact/contact.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'checklist',
            'title' => __('Checklist'),
            'description' => __('Checklist block.'),
            'render_template' => 'partials/blocks/checklist/checklist.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'gallery-slider',
            'title' => __('Gallery Slider'),
            'description' => __('Gallery slider block.'),
            'render_template' => 'partials/blocks/gallery-slider/gallery-slider.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'members-archive',
            'title' => __('Members Archive'),
            'description' => __('Members Archive / Tabs.'),
            'render_template' => 'partials/blocks/members-archive/members-archive.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'blog-archive',
            'title' => __('Blog Archive'),
            'description' => __('Blog Archive / Tabs.'),
            'render_template' => 'partials/blocks/blog-archive/blog-archive.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'image-copy-repeater',
            'title' => __('Image Copy'),
            'description' => __('Alternating image and copy blocks.'),
            'render_template' => 'partials/blocks/image-copy/image-copy.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'image-copy-repeater',
            'title' => __('Image Copy'),
            'description' => __('Alternating image and copy blocks.'),
            'render_template' => 'partials/blocks/image-copy/image-copy.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'insights-intro',
            'title' => __('Insights intro'),
            'description' => __('Checklist and description block.'),
            'render_template' => 'partials/blocks/insights-intro/insights-intro.php'
        )
    );

    acf_register_block_type(
        array(
            'name' => 'centralised-copy',
            'title' => __('Centralised Copy'),
            'description' => __('Centralised Copy'),
            'render_template' => 'partials/blocks/centralised-copy/centralised-copy.php'
        )
    );
}

if (function_exists('acf_register_block_type')):
    add_action('acf/init', 'register_acf_block_types');
endif;
