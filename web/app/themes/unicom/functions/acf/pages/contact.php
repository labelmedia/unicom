<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['contact_settings'] = new FieldsBuilder('contact');

$fieldGroups['contact_settings']
    ->setLocation('post_template', '==', 'page-contact.php')
    ->addText('heading')
    ->addNumber('gravity_form_id', [
        'label' => 'Form ID',
        'required' => true
    ])
    ->setGroupConfig('hide_on_screen', [
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);