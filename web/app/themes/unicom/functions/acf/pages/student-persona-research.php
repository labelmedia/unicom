<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['spr_settings'] = new FieldsBuilder('student-persona-research');

$fieldGroups['spr_settings']
    ->setLocation('post_template', '==', 'template-insight-landing.php')
    ->addImage('report_image', [
        'label' => 'Report Image',
        'required' => true
    ])
    ->addText('form_title', [
        'label' => 'Form Title',
        'instructions' => 'If not title is entered, "Download our report" will be displayed.'
    ])
    ->addNumber('gravity_form_id', [
        'label' => 'Form ID',
        'required' => true
    ])
    ->setGroupConfig('hide_on_screen', [
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);
