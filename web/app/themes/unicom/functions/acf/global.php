<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['global_content'] = new FieldsBuilder('global_content');

$fieldGroups['global_content']
->addTab('General', [
    'placement' => 'left'
])
->addLink('Phone')
->addLink('Email')
->setLocation('options_page', '==', 'global-content')
->addRepeater('global_navigation', [
    'label' => 'Navigation',
    'instructions' => 'Found in site header on all viewports.',
    'button_label' => 'New Nav Item'
])
    ->addTrueFalse('logged_in_only')
    ->addLink('page_link')
->endRepeater()
->addRepeater('footer_logos')
    ->addImage('image')
->endRepeater()
->addRepeater('footer_nav', [
    'label' => 'Footer Navigation'
])
    ->addLink('link')
->endRepeater()
->addRepeater('footer_social_icons')
    ->addUrl('url', [
        'required' => true
    ])
    ->addImage('image', [
        'return_format' => 'url',
        'required' => true
    ])
    ->addText('css_modifier', [
        'label' => 'BEM modifier for icon sizing',
        'required' => true,
        'prepend' => '--'
    ])
->endRepeater()
->addTab('Webinar Widget', [
    'placement' => 'left'
])
    ->addPostObject('webinar_widget_override', [
        'instructions' => 'Manually set the webinar pulled through into the countdown widget. Use the small x to remove. Will hide after webinar event start.',
        'multiple' => 0,
        'post_type' => 'webinars',
        'return_format' => 'id',
        'allow_null' => true
    ])
->addTab('Contact Block', [
    'placement' => 'left'
])
    ->addTextarea('global_contact_heading', [
        'new_lines' => 'br'
    ])
    ->addNumber('global_gravity_id', [
        'label' => 'Gravity Forms ID'
    ])
->setGroupConfig('hide_on_screen', [
    'permalink',
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'revisions',
    'slug',
    'author',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackbacks'
]);
