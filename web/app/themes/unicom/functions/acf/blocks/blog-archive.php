<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['members_archive'] = new FieldsBuilder('Members Archive');

$fieldGroups['members_archive']
    ->addMessage('', 'Tabs are pulled through via custom post types.')
    ->setLocation('block', '==', 'acf/members-archive');