<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['columns'] = new FieldsBuilder('Columns');

$fieldGroups['columns']
    ->addField('copy','medium_editor', [
        'label' => 'Copy',
        'standard_buttons' => [],
        'custom_buttons' => [
            [
                'name' => 'highlighted',
                'label' => 'Highlighted',
                'tag' => 'span',
                'attributes' => [
                    [
                        'name' => 'class',
                        'value' => 'utl-highlighted'
                    ]
                ]
            ]
        ],
        'other_options' => [
            'allowBreakInSingleLineInput' => 'allowBreakInSingleLineInput',
            'disableDoubleReturn' => 'disableDoubleReturn',
            'disableExtraSpaces' => 'disableExtraSpaces'
        ]
    ])
    ->addRepeater('columns')
        ->addImage('image')
        ->addField('copy','medium_editor', [
            'label' => 'Copy',
            'standard_buttons' => [],
            'custom_buttons' => [
                [
                    'name' => 'highlighted',
                    'label' => 'Highlighted',
                    'tag' => 'span',
                    'attributes' => [
                        [
                            'name' => 'class',
                            'value' => 'utl-highlighted'
                        ]
                    ]
                ]
            ],
            'other_options' => [
                'allowBreakInSingleLineInput' => 'allowBreakInSingleLineInput',
                'disableDoubleReturn' => 'disableDoubleReturn',
                'disableExtraSpaces' => 'disableExtraSpaces'
            ]
        ])
        ->addLink('link')
        ->addTrueFalse('hide_copy_mobile', [
            'label' => 'Reduce Mobile Content',
            'instructions' => 'When checked, the "Copy" and "Link" elements will be hidden on mobile.',
            'default_value' => false
        ])
    ->setLocation('block', '==', 'acf/columns');
