<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['intro'] = new FieldsBuilder('Intro');

$fieldGroups['intro']
    ->setLocation('block', '==', 'acf/intro')
    ->addTextarea('copy', [
        'new_lines' => 'br'
    ])
    ->addLink('link');