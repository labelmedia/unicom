<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['blog_archive'] = new FieldsBuilder('Blog Archive');

$fieldGroups['blog_archive']
    ->addMessage('', 'Tabs are pulled through via custom post types.')
    ->setLocation('block', '==', 'acf/blog-archive');