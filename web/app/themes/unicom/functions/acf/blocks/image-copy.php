<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['image_copy_block'] = new FieldsBuilder('Image / Copy');

$fieldGroups['image_copy_block']
    ->addRepeater('content')
        ->addImage('image')
        ->addText('heading')
        ->addTextarea('content')
    ->endRepeater()
    ->setLocation('block', '==', 'acf/image-copy-repeater');