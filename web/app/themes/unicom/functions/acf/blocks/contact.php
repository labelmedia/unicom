<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['contact'] = new FieldsBuilder('contact');

$fieldGroups['contact']
    ->addMessage('','Please go to WordPress Dashboard -> Theme Settings for contact block fields.')
    ->setLocation('block', '==', 'acf/contact');