<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['checklist'] = new FieldsBuilder('Checklist');

$fieldGroups['checklist']
    ->addTextarea('heading', [
        'new_lines' => 'br'
    ])
    ->addRepeater('checklist')
        ->addText('text')
    ->endRepeater()
    ->addLink('link')
    ->addPostObject('webinar', [
        'multiple' => 0,
        'post_type' => 'webinars',
        'return_format' => 'id'
    ])
    ->addImage('image_one', [
        'instructions' => 'Floating image #1',
        'return_format' => 'url'
    ])
    ->addImage('image_two', [
        'instructions' => 'Floating image #2',
        'return_format' => 'url'
    ])
    ->addImage('image_three', [
        'instructions' => 'Floating image #3',
        'return_format' => 'url'
    ])
    ->setLocation('block', '==', 'acf/checklist');