<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['gallery_slider'] = new FieldsBuilder('gallery_slider');

$fieldGroups['gallery_slider']
    ->setLocation('block', '==', 'acf/gallery-slider')
    ->addText('heading')
    ->addRepeater('gallery_slider', [
    'button_label' => 'Add Slide'
    ])
        ->addText('heading')
        ->addTextarea('content')
        ->addGroup('background_image')
            ->addImage('desktop')
            ->addImage('mobile')
        ->endGroup()
    ->endRepeater();