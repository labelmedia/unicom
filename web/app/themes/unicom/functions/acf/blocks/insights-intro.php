<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['insights_intro'] = new FieldsBuilder('insights_intro');

$fieldGroups['insights_intro']
    ->addField('heading','medium_editor', [
        'label' => 'Copy',
        'standard_buttons' => [],
        'custom_buttons' => [
            [
                'name' => 'highlighted',
                'label' => 'Highlighted',
                'tag' => 'span',
                'attributes' => [
                    [
                        'name' => 'class',
                        'value' => 'utl-highlighted'
                    ]
                ]
            ]
        ],
        'other_options' => [
            'allowBreakInSingleLineInput' => 'allowBreakInSingleLineInput',
            'disableDoubleReturn' => 'disableDoubleReturn',
            'disableExtraSpaces' => 'disableExtraSpaces'
        ]
    ])
    ->addRepeater('checklist')
        ->addText('text')
    ->endRepeater()
    ->setLocation('block', '==', 'acf/insights-intro');