<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['copy_block'] = new FieldsBuilder('Copy');

$fieldGroups['copy_block']
    ->setLocation('block', '==', 'acf/copy-block')
    ->addRepeater('content')
        ->addText('heading')
        ->addField('copy','medium_editor', [
            'label' => 'Copy',
            'standard_buttons' => [],
            'custom_buttons' => [
                [
                    'name' => 'highlighted',
                    'label' => 'Highlighted',
                    'tag' => 'span',
                    'attributes' => [
                        [
                            'name' => 'class',
                            'value' => 'utl-highlighted'
                        ]
                    ]
                ]
            ],
            'other_options' => [
                'allowBreakInSingleLineInput' => 'allowBreakInSingleLineInput',
                'disableDoubleReturn' => 'disableDoubleReturn',
                'disableExtraSpaces' => 'disableExtraSpaces'
            ]
        ])
    ->endRepeater();