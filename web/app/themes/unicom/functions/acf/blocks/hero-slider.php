<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['hero_slider'] = new FieldsBuilder('Hero Slider');

$fieldGroups['hero_slider']
    ->setLocation('block', '==', 'acf/hero-slider')
    ->addRepeater('hero_slider', [
        'button_label' => 'Add Slide'
    ])
        ->addGroup('background_image')
            ->addImage('desktop')
            ->addImage('mobile')
        ->endGroup()
        ->addText('sub_heading')
        ->addTextarea('heading', [
            'new_lines' => 'br'
        ])
        ->addText('copy')
        ->addLink('cta')
    ->endRepeater()
    ->addTrueFalse('has_widget',[
        'label' => 'Has webinar countdown widget?'
    ])
    ->addTrueFalse('has_arrow',[
        'label' => 'Has arrow in circle (bottom left)?'
    ]);