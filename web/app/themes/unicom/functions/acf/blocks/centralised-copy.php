<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['centralised_copy'] = new FieldsBuilder('Centralised Copy');

$fieldGroups['centralised_copy']
    ->setLocation('block', '==', 'acf/centralised-copy')
    ->addField('copy','medium_editor', [
        'label' => 'Copy',
        'standard_buttons' => [],
        'custom_buttons' => [
            [
                'name' => 'highlighted',
                'label' => 'Highlighted',
                'tag' => 'span',
                'attributes' => [
                    [
                        'name' => 'class',
                        'value' => 'utl-highlighted'
                    ]
                ]
            ]
        ],
        'other_options' => [
            'allowBreakInSingleLineInput' => 'allowBreakInSingleLineInput',
            'disableDoubleReturn' => 'disableDoubleReturn',
            'disableExtraSpaces' => 'disableExtraSpaces'
        ]
    ]);
