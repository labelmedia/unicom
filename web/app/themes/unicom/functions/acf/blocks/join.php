<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['join'] = new FieldsBuilder('join');

$fieldGroups['join']
    ->setLocation('block', '==', 'acf/join')
    ->addTextarea('copy', [
        'new_lines' => 'br'
    ])
    ->addLink('link');