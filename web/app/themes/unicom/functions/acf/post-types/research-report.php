<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['white_papers_settings'] = new FieldsBuilder('Report Settings');
$fieldGroups['white_papers_settings']
    ->addFile('download', [
        'mime_types' => 'pdf'
    ])
    ->addLink('report_landing_page',[
        'label' => 'Report Landing Page'
    ])
    ->setLocation('post_type', '==', 'insight-and-research')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'slug',
        'author',
        'format',
        'categories',
        'tags',
        'send-trackbacks',
    ]);
