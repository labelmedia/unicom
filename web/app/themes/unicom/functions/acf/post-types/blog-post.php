<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['blog_hero'] = new FieldsBuilder('Post Hero Settings');

$fieldGroups['blog_hero']
    ->setLocation('post_type', '==', 'post')
    ->addText('author', [
        'instructions' => 'Post Author (displayed after hero)'
    ])
    ->addGroup('background_image')
        ->addImage('desktop')
        ->addImage('mobile')
    ->endGroup();
