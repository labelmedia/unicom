<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['card_settings'] = new FieldsBuilder('card_settings');
$fieldGroups['card_settings']
    ->addTrueFalse('available_logged_out', [
        'Available logged out?'
    ])
    ->addImage('preview_image', [
        'return_format' => 'url'
    ])
    ->setLocation('post_type', '==', 'webinars')
        ->or('post_type', '==', 'case-studies')
        ->or('post_type', '==', 'research-reports')
        ->or('post_type', '==', 'insight-and-research-reports')
        ->or('post_type', '==', 'post')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'slug',
        'author',
        'format',
        'categories',
        'tags',
        'send-trackbacks',
    ]);
