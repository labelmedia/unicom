<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['case_study_settings'] = new FieldsBuilder('case_study_settings');
$fieldGroups['case_study_settings']
    ->addRepeater('hero_slider', [
        'button_label' => 'Add Slide'
    ])
        ->addGroup('background_image')
            ->addImage('desktop')
            ->addImage('mobile')
        ->endGroup()
        ->addTextarea('heading', [
            'new_lines' => 'br'
        ])
        ->addText('copy')
    ->endRepeater()
    ->addImage('hero_background')
    ->addRepeater('stats', [
        'max' => 3
    ])
        ->addText('number')
        ->addTextarea('description', [
            'new_lines' => 'br'
        ])
    ->endRepeater()
    ->setLocation('post_type', '==', 'case-studies')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'slug',
        'author',
        'format',
        'categories',
        'tags',
        'send-trackbacks',
    ]);
