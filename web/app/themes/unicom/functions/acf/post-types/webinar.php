<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fieldGroups['webinar_settings'] = new FieldsBuilder('webinar_settings');
$fieldGroups['webinar_settings']
    ->addDateTimePicker('webinar_datetime', [
        'required' => 'required',
        'label' => 'Webinar Date/Time',
        'return_format' => 'Y-m-d H:i:s'
    ])
    ->addUrl('link', [
        'label' => 'Registration url',
        'instructions' => 'To be shown before the Webinar Date/Time is passed'
    ])
    ->addOembed('video', [
        'instructions' => 'Video of webinar, field to only be pulled through after the Webinar Date/Time field has been reached'
    ])
    ->addNumber('minutes', [
        'label' => 'Webinar video time',
        'append' => 'mins'
    ])
    ->setLocation('post_type', '==', 'webinars')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'slug',
        'author',
        'format',
        'categories',
        'tags',
        'send-trackbacks',
    ]);
