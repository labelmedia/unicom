<?php

// Define Global Options
if(function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'Theme Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'global-content',
        'capability' => 'create_users',
        'redirect' => false
    ]);
}

// Hide menu on production env
//if(ENV == 'production') {
//    add_filter('acf/settings/show_admin', '__return_false');
//}
