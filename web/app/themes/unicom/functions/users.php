<?php

// Create new client user role and rename it to 'Administrator'
add_action('init', function() {
    // Get default 'Admin' user role
    $admin_role_set = get_role('administrator')->capabilities;

    // Create new client user role with default 'Administrator' caps
    add_role('marv_client', 'Administrator', $admin_role_set);
});

// Hide ACF menu to non-Administrators
add_filter('acf/settings/show_admin', function($show) {
    $current_user = wp_get_current_user();

    if(in_array('administrator', $current_user->roles)) {
        return true;
    }
});

// Remove Administrator role from roles list for non-Administrators
add_action('editable_roles', function($roles) {
    $current_user = wp_get_current_user();

    if(isset($roles['administrator']) && !in_array('administrator', $current_user->roles)) {
        unset($roles['administrator']);
    }

    return $roles;
});

// Remove caps from client role
add_action('init', function() {
    $role = get_role('marv_client');

    $caps = [
        'install_plugins',
        'edit_plugins',
        'activate_plugins',
        'delete_plugins',
        'update_plugins'
    ];

    foreach($caps as $cap) {
        $role->remove_cap($cap);
    }
});

// Assign Marvellous users to Administrator role by default
add_action('user_register', function($user_id) {
    // Get user
    $user = get_user_by('id', $user_id);

    // Get user email address
    $domain = substr(
        strrchr(
            $user->data->user_email,
            "@"
        ), 1
    );

    // Get user username
    $username = $user->data->user_login;

    // Define targeted usernames
    $usernames = [
        'marvellous-admin'
    ];

    // Define targeted email addresses
    $contributor_domains = [
        'wearemarvellous.com',
        'marvellous.digital'
    ];

    // If user has defined username or email address, assign them to the 'Administrator' role
    if(in_array($domain, $contributor_domains) || in_array($username, $usernames)) {
        foreach($user->roles as $role) {
            $user->remove_role($role);
        }

        $user->add_role('administrator');
    }
});

// Rename client admin user just for Administrators
add_action('init', function() {
    global $wp_roles;
    $current_user = wp_get_current_user();

    if(!isset($wp_roles)) {
        $wp_roles = new WP_Roles();
    }

    if(in_array('administrator', $current_user->roles)) {
        $wp_roles->roles['marv_client']['name'] = 'Client Admin';
        $wp_roles->role_names['marv_client'] = 'Client Admin';
    }
});

// Grant Ninja Forms access to Editors
add_filter('ninja_forms_admin_parent_menu_capabilities', 'nf_subs_capabilities');
add_filter('ninja_forms_admin_all_forms_capabilities', 'nf_subs_capabilities');
add_filter('ninja_forms_admin_submissions_capabilities', 'nf_subs_capabilities');
function nf_subs_capabilities($cap) {
    return 'edit_posts';
};

// Only show Jetpack to Administrators
add_action('admin_init', function() {
    if(class_exists('Jetpack') && !current_user_can('manage_options')) {
        remove_menu_page('jetpack');
    }
});
