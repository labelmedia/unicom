<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://ponderosa.agency/
 * @since      1.0.0
 *
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/admin
 * @author     Matt Day <matthewd@ponderosa.agency>
 */
class Ac_Branded_Elements_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->admin_slug = 'ac-elements';
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {


		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'dist/css/ac-branded-elements-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {


		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'dist/js/ac-branded-elements-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function display_notice($type, $message){
		$class = 'notice notice-'.$type;
		printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
	}

	public function register_admin_menu()
	{
		add_menu_page(
			__('AC Elements Settings', $this->plugin_name),
			'AC Elements',
			'manage_options',
			$this->admin_slug,
			array($this, 'display_admin_page'),
			'',
			6
		);

	}

	public function display_admin_page()
	{
		require_once __DIR__ .'/partials/ac-branded-elements-admin-display.php';
	}

	public static function get_options($option){
		$options = get_option('ac-elements-settings');
		return $options[$option];
	}

	public function register_settings() {
		register_setting('ac-elements-settings','ac-elements-settings');
		$this->register_settings_sections();
		$this->register_settings_fields('ac-elements-display-settings');
	}

	public function register_settings_sections()
	{
		add_settings_section('ac-elements-display-settings','Display Settings', function(){} ,$this->admin_slug);
	}

	public function register_settings_fields($section)
	{
		$fields = [];

		switch ($section) :
			case 'ac-elements-display-settings':
				$fields = [
					array(
						'name' => 'display-global-footer',
						'label' => 'Automatically Display Global Footer',
						'page' => $this->admin_slug,
						'section' => $section,
					),
					array(
						'name' => 'global-footer-theme',
						'label' => 'Global Footer Style ',
						'page' => $this->admin_slug,
						'section' => $section,
					),
				];
				break;
		endswitch;

		foreach ($fields as $field) :
			add_settings_field($field['name'],$field['label'], array($this, 'render_settings_field'),$this->admin_slug, $field['section'], array(
				'field_name' => $field['name']
			));
		endforeach;
	}

	public function render_settings_field($args)
	{
		require_once __DIR__ .'/partials/fields/ac-elements-'.$args['field_name'].'.php';
	}

}
