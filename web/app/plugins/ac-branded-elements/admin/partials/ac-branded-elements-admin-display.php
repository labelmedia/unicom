<div class="wrap">
    <h1>AC Elements Display Settings Page</h1>
    <?php
        if(isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true'):
            $this->display_notice('success', 'Display settings saved successfully!');
        endif;
    ?>
    <form id="ac-elements-settings-form" method="post" action="options.php">
        <?php settings_fields('ac-elements-settings'); ?>
        <?php do_settings_sections($this->admin_slug); ?>
        <p class="submit">
            <input name="submit" type="submit" class="button-primary" value="Save Changes" />
        </p>
    </form>
</div>
