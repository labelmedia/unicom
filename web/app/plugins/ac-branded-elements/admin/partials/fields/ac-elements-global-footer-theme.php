<?php

$themes = ['black', 'red', 'green', 'white'];

?>

<select name="ac-elements-settings[global-footer-theme]">
    <?php foreach($themes as $theme):?>
        <option <?= $theme == $this->get_options('global-footer-theme') ? 'selected' : '';?> value="<?= $theme;?>"><?= ucfirst($theme);?></option>
    <?php endforeach;?>
</select>