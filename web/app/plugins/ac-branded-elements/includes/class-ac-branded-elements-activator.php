<?php

/**
 * Fired during plugin activation
 *
 * @link       https://ponderosa.agency/
 * @since      1.0.0
 *
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/includes
 * @author     Matt Day <matthewd@ponderosa.agency>
 */
class Ac_Branded_Elements_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
