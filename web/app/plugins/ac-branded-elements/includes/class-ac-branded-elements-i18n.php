<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://ponderosa.agency/
 * @since      1.0.0
 *
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/includes
 * @author     Matt Day <matthewd@ponderosa.agency>
 */
class Ac_Branded_Elements_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ac-branded-elements',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
