<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://ponderosa.agency/
 * @since      1.0.0
 *
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/includes
 * @author     Matt Day <matthewd@ponderosa.agency>
 */
class Ac_Branded_Elements_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
