<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://ponderosa.agency/
 * @since      1.0.0
 *
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ac_Branded_Elements
 * @subpackage Ac_Branded_Elements/public
 * @author     Matt Day <matthewd@ponderosa.agency>
 */
class Ac_Branded_Elements_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ac_Branded_Elements_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ac_Branded_Elements_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, $this->get_public_url() . 'dist/css/ac-branded-elements-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ac_Branded_Elements_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ac_Branded_Elements_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, $this->get_public_url() . 'dist/js/ac-branded-elements-public.js', array( 'jquery' ), $this->version, false );

		wp_localize_script($this->plugin_name, 'ac_elements', array(
			'footer' => [
				'display' => boolval(get_option('ac-elements-settings')['display-global-footer'] === 'on'),
				'element' => $this->get_element_markup('footer', get_option('ac-elements-settings')['global-footer-theme']),
			]
		));

	}

	public function get_element_markup(string $element, string $theme = null)
	{
		ob_start();

		if($theme):
			$atts = array(
				'theme' => $theme
			);
		endif;

		require $this->get_public_path().'partials/elements/'.$element.'.php';

		$content = ob_get_contents();

		ob_end_clean();

		return $content;
	}

	public function add_shortcodes()
	{
		add_shortcode('ac-footer', array($this, 'do_ac_footer'));
	}

	public function do_ac_footer($atts)
	{
		$atts = shortcode_atts(array(
			'theme' => 'black',
		), $atts, 'ac-footer');

		require $this->get_public_path().'partials/elements/footer.php';
	}

	public function get_public_url()
	{
		return plugin_dir_url( __FILE__ );
	}

	public function get_public_path()
	{
		return plugin_dir_path( __FILE__ );
	}

}
