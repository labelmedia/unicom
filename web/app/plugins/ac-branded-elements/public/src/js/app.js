import $ from 'jquery';

import Footer from './elements/footer';

$(document).ready(() => {

    if (ac_elements.footer.display == '1'){
        Footer.insert(ac_elements.footer.element);
    }
});