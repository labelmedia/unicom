<?php

$theme = isset($atts) ? $atts['theme'] : 'black';

?>

<div class="ac-elements-footer t-<?=$theme;?>">
    <div class="ac-elements-footer__wrapper">
        <div class="ac-elements-footer__logo">
            <a target="_blank" rel="noopener" href="https://thisisaudience.com">
                <img src="<?= $this->get_public_url().'dist/images/logo-'.$theme.'.svg';?>" alt="Audience Collective logo" title="Audience Collective logo" loading="lazy" />
            </a>
        </div>

        <div class="ac-elements-footer__copy">
            An insight-driven group of <strong>specialist agencies</strong> that helps <strong>brands</strong> better connect with their <strong>audience</strong>.
        </div>
    </div>
</div>