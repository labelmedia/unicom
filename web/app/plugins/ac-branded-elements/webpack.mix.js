const mix = require('laravel-mix');

mix.sass('admin/src/scss/style.scss', 'admin/dist/css/ac-branded-elements-admin.css')
    .options({
        processCssUrls: false
    });

mix.sass('public/src/scss/style.scss', 'public/dist/css/ac-branded-elements-public.css')
    .options({
        processCssUrls: false
    });

mix.js('public/src/js/app.js', 'public/dist/js/ac-branded-elements-public.js');

mix.copy('public/src/images/*', 'public/dist/images');