<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://ponderosa.agency/
 * @since             1.0.2
 * @package           Ac_Branded_Elements
 *
 * @wordpress-plugin
 * Plugin Name:       Audience Collective Branded Elements
 * Plugin URI:        https://thisisaudience.com/
 * Description:       Use Audience Collective branded elements on your Wordpress website.
 * Version:           1.0.2
 * Author:            Matt Day
 * Author URI:        https://ponderosa.agency/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ac-branded-elements
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'AC_BRANDED_ELEMENTS_VERSION', '1.0.2' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ac-branded-elements-activator.php
 */
function activate_ac_branded_elements() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ac-branded-elements-activator.php';
	Ac_Branded_Elements_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ac-branded-elements-deactivator.php
 */
function deactivate_ac_branded_elements() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ac-branded-elements-deactivator.php';
	Ac_Branded_Elements_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ac_branded_elements' );
register_deactivation_hook( __FILE__, 'deactivate_ac_branded_elements' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ac-branded-elements.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ac_branded_elements() {

	$plugin = new Ac_Branded_Elements();
	$plugin->run();

}
run_ac_branded_elements();
