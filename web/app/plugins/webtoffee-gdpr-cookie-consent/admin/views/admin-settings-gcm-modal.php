<?php
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}
$terms = Cookie_Law_Info_Cookies::get_instance()->get_cookie_category_options();
$cat_options = get_option('cli_gcm_categories',array ( 'necessary' => 'necessary' ,'functional' => 'functional', 'advertisement' => 'advertisement', 'analytics' => 'analytics' ));
?>
<div id="cli_gcm_category_modal" class="cli_modal">
    <div class="cli_modal-content">
        <div  class="cli_modal_header">
            <span class="cli_gcm_cat_header"><?php esc_html_e('Consent type mapping', 'webtoffee-gdpr-cookie-consent'); ?></span>
            <span class="cli_close">&times;</span>
        </div>
        <div class="cli_modal_content">
            <span class="cli_gcm_cat_desc"><?php esc_html_e('Each consent type needs to be mapped to a certain cookie category', 'webtoffee-gdpr-cookie-consent'); ?></span>
            <table class="cli_gcm_cat_table">
                <tr>
                    <td><span class="cli_header_row"><?php esc_html_e('Google consent type', 'webtoffee-gdpr-cookie-consent'); ?></span></td>
                    <td><span class="cli_header_row"><?php esc_html_e('Cookie category', 'webtoffee-gdpr-cookie-consent'); ?></span></td>
                </tr>
                <tr>
                    <td>
                        <span class="cli_necessary_row cli_gcm_cat_title"><?php esc_html_e('Necessary', 'webtoffee-gdpr-cookie-consent'); ?></span>
                        <div class="cli_gtag_cat"><?php esc_html_e('security_storage', 'webtoffee-gdpr-cookie-consent'); ?></div>
                    </td>
                    <td>
                    <select class="cli_gcm_category cli_gcm_select" name="cli_gcm_category" id="cli_necessary_row">
                        <option value="<?php echo esc_attr('0'); ?>"><?php echo esc_html__('Choose cookie category', 'webtoffee-gdpr-cookie-consent'); ?>
                        </option>
                        <?php foreach ($terms as $key => $term) : ?>
                            <option value="<?php echo esc_attr( $key ); ?>" data-tag-key="<?php echo esc_attr('necessary'); ?>" <?php echo selected( esc_attr( $cat_options['necessary']), esc_attr( $key ), false ); ?>>
                                <?php echo esc_html($term['title']); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="cli_functional_row cli_gcm_cat_title"><?php _e('Functional', 'webtoffee-gdpr-cookie-consent'); ?></span>
                        <div class="cli_gtag_cat"><?php esc_html_e('functionality_storage', 'webtoffee-gdpr-cookie-consent'); ?></div>
                        <div class="cli_gtag_cat"><?php esc_html_e('personalization_storage', 'webtoffee-gdpr-cookie-consent'); ?></div>
                    </td>
                    <td>
                        <select class="cli_gcm_category cli_gcm_select" name="cli_gcm_category" id="cli_functional_row">
                            <option value="<?php echo esc_attr('0'); ?>"><?php echo esc_html__('Choose cookie category', 'webtoffee-gdpr-cookie-consent'); ?></option>
                            <?php foreach ($terms as $key => $term) : ?>
                                <option value="<?php echo esc_attr( $key ); ?>" data-tag-key="<?php echo esc_attr__('functional', 'webtoffee-gdpr-cookie-consent'); ?>" <?php selected( esc_attr( $cat_options['functional'] ), esc_attr( $key ) ); ?>>
                                    <?php echo esc_html($term['title']); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="cli_advertisement_row cli_gcm_cat_title"><?php _e('Advertisement', 'webtoffee-gdpr-cookie-consent'); ?></span>
                        <div class="cli_gtag_cat"><?php esc_html_e('ad_storage', 'webtoffee-gdpr-cookie-consent'); ?></div>
                        <div class="cli_gtag_cat"><?php esc_html_e('ad_user_data', 'webtoffee-gdpr-cookie-consent'); ?></div>
                        <div class="cli_gtag_cat"><?php esc_html_e('ad_personalization', 'webtoffee-gdpr-cookie-consent'); ?></div>

                    </td>
                    <td>
                        <select class="cli_gcm_category cli_gcm_select" name="cli_gcm_category" id="cli_advertisement_row">
                            <option value="<?php echo esc_attr('0'); ?>"><?php echo esc_html__('Choose cookie category', 'webtoffee-gdpr-cookie-consent'); ?></option>
                            <?php foreach ($terms as $key => $term) : ?>
                                <option value="<?php echo esc_attr( $key ); ?>" data-tag-key="<?php echo esc_attr__('advertisement', 'webtoffee-gdpr-cookie-consent'); ?>" <?php echo selected( esc_attr( $cat_options['advertisement'] ), esc_attr( $key ), false); ?>>
                                    <?php echo esc_html($term['title']); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="cli_analytics_row cli_gcm_cat_title"><?php _e('Analytics', 'webtoffee-gdpr-cookie-consent'); ?></span>
                        <div class="cli_gtag_cat"><?php esc_html_e('analytics_storage', 'webtoffee-gdpr-cookie-consent'); ?></div>
                    </td>
                    <td>
                        <select class="cli_gcm_category cli_gcm_select" name="cli_gcm_category" id="cli_analytics_row">
                            <option value="<?php echo esc_attr('0'); ?>"><?php echo esc_html__('Choose cookie category', 'webtoffee-gdpr-cookie-consent'); ?></option>
                            <?php foreach ($terms as $key => $term) : ?>
                                <option value="<?php echo esc_attr( $key ); ?>" data-tag-key="<?php echo esc_attr__('analytics', 'webtoffee-gdpr-cookie-consent'); ?>" <?php selected( esc_attr( $cat_options['analytics'] ), esc_attr( $key ) ); ?>>
                                    <?php echo esc_html($term['title']); ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div  class="cli_modal_footer">
            <span class="cli_gcm_action_btn" id="cli_cancel_btn"><?php _e('Cancel', 'webtoffee-gdpr-cookie-consent'); ?></span>
            <span class="cli_gcm_action_btn" id="cli_submit_btn"><?php _e('Confirm', 'webtoffee-gdpr-cookie-consent'); ?></span>
        </div>
    </div>
</div>
<style>
    /* Add this in your <style> or CSS file */
    .cli_modal {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5); /* Semi-transparent background */
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 9999;
        overflow-y: auto;
    }

    .cli_modal-content {
        background-color: #fff;
        padding: 20px 40px 20px 40px;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        height: 566px;
        width: 652px;
    }
    .cli_close {
        position: absolute;
        right: 0;
        top: 10px;
        cursor: pointer;
        font-size: 20px;
        color: #555E6B; /* Set color according to your design */
    }
    .cli_modal_header,.cli_modal_footer {
        height: 60px;
        position: relative;
    }
    .cli_modal_content {
       min-height: 436px;
    }
    .cli_gcm_cat_table {
        width:100%;
        height: 373px;
        border-radius: 9px;
        border: 1px solid #EAEBED;
        border-spacing: unset;
        margin-top: 10px;
    }
    .cli_gcm_cat_table tbody tr:first-child, .cli_gcm_cat_table tbody tr:first-child td{
        border-top-left-radius: 9px;
        border-top-right-radius: 9px;
    }
    .cli_gcm_cat_table tbody tr:last-child, .cli_gcm_cat_table tbody tr:last-child td{
        border-bottom-left-radius: 9px;
        border-bottom-right-radius: 9px;
    }
    .cli_gtag_cat {
        font-size: 12px;
        font-weight: 400;
        font-style: italic;
    }
    .cli_gcm_cat_table tr:nth-child(even) {
        background-color: #F6F7F8; /* Set background color for even rows */
    }
    .cli_gcm_cat_table tr:nth-child(odd) {
        background-color: #FFFFFF; /* Set background color for odd rows */
    }
    .cli_gcm_cat_table tr td:nth-child(2) .cli_gcm_select{
        width:90% !important;
        float: left;
    }
    .cli_gcm_cat_table tr td:nth-child(1){
        width:45%;
    }
    .cli_gcm_cat_table td {
        padding: 20px 30px;
        border: none;
    }
    .cli_gcm_cat_title {
        font-size: 14px;
        font-weight: 500;
        line-height: 16px;
        color: #555E6B;
    }
    .cli_gcm_cat_desc {
        font-size: 14px;
        font-weight: 400;
        line-height: 16px;
        color: #2A3646;
    }
    .cli_header_row {
        font-size: 14px;
        font-weight: 700;
        color: #6E7681;
    }
    .cli_gcm_cat_header {
        font-size: 16px;
        font-weight: 600;
        line-height: 28px;
        color: #2A3646;
    }
    .cli_modal_footer {
        display: flex;
        align-items: end;
        justify-content: end;
    }
    .cli_gcm_action_btn {
        font-size: 14px;
        font-weight: 600;
        line-height: 16px;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
    }
    #cli_cancel_btn {
        color: #056BE7;
        background-color: #FFFFFF;
    }
    #cli_submit_btn {
        color: #FFFFFF;
        background-color: #3157A6;
    }
    .cli_gcm_mapping_link {
        color: #056BE7;
        font-weight: 600;
        font-size: 14px;
        text-decoration: underline;
    }
    .cli_gcm_cat_table .cli_gcm_category {
        font-size: 14px !important;
        width: 100% !important;
    }
    .wt-cli-warning-icon {       
        margin-left: 6px;
        margin-top: 6px;
    }
</style>