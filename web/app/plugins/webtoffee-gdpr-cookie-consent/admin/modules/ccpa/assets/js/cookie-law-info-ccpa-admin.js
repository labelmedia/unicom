jQuery( function($) {

    var ccpaEnabledState = jQuery('input[name="ccpa_enabled_field"]:checked').val();
    var geoIP = jQuery('input[name="is_eu_on_field"]:checked').val();
    function changeMessage( $event = false ) {
        var message = '';
        var toggler = jQuery('input[type="radio"][name="consent_type_field"]:checked');
        var consentType = toggler.val();
        var ccpaSettingsEnabled = false;
        var gdprEnabled = true;
        var toggleTarget = toggler.attr('data-cli-toggle-target');
        var iab_row = jQuery('#cli_enable_iab').parents('tr');

        jQuery('.wt-cli-section-gdpr-ccpa .wt-cli-section-inner').hide();
        jQuery('.wt-cli-toggle-content').hide();

        if (consentType == 'ccpa') {
            message = jQuery('#wt_ci_ccpa_only').val();
            ccpaSettingsEnabled = true;
            gdprEnabled = false;
            iab_row.hide();
        }
        else if ( consentType == 'ccpa_gdpr') {
            message = jQuery('#wt_ci_ccpa_gdpr').val();
            jQuery('.wt-cli-section-gdpr-ccpa .wt-cli-section-inner').show();
            ccpaSettingsEnabled = true;
            iab_row.show();
        } 
        else { 
            message = jQuery('#wt_ci_gdpr_only').val(); 
            ccpaSettingsEnabled = false;
            iab_row.show();
        }
        jQuery.ajax({
            url: cli_admin.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: '&wt_cli_consent_type=' + consentType + '&action=wt_cli_update_consent_type&_wpnonce=' + cli_admin.nonce,
            success: function (data)
            {
                
            },
        });
        jQuery('textarea[name="notify_message_field"]').val( message );
        jQuery('.wt-cli-section-gdpr-ccpa .wt-cli-section-inner-'+consentType).show();
        if( ccpaSettingsEnabled === false ) {
            jQuery('.wt-cli-ccpa-element').hide();
            jQuery('input[name="ccpa_enabled_field"]').prop("checked",false);
        }
        else {
            jQuery('.wt-cli-ccpa-element').show();
            jQuery('input[name="ccpa_enabled_field"][value="'+ccpaEnabledState+'"]').prop('checked', true);
            if( $event === true ) {
                jQuery('input[name="ccpa_enabled_field"]').prop("checked",true);
            }
            
        }
        if( gdprEnabled === false ) {
            jQuery('input[name="is_eu_on_field"][value="false"]').prop('checked', true);
        }
        else {
            jQuery('input[name="is_eu_on_field"][value="'+geoIP+'"]').prop('checked', true);
        }
        jQuery('.wt-cli-toggle-content[data-cli-toggle-id='+toggleTarget+']').show();
    }
    changeMessage();
    jQuery('input[type="radio"][name="consent_type_field"]').change(function() {
        changeMessage(true);
    });
    // show/hide IAB notice.
    var consentType = jQuery('input[type="radio"][name="consent_type_field"]:checked').val();
    jQuery('.cli_iab_notice').hide();
    jQuery('.wt_cli_enable_consent_mode').hide();
    if(jQuery('[name="cli_enable_iab"]').is(':checked') && 'ccpa' !== consentType)
    {
        jQuery('.cli_iab_notice').show();
        jQuery('.wt_cli_enable_consent_mode').show();
        jQuery('.cli_theme_customize .cli_theme_customizebutton , .cli_theme_customize .cli_theme_customizebox , .cli_theme_customize div textarea').css({'opacity':0.5,'pointer-events':'none'});
    }
    jQuery('input[type=checkbox][name=cli_enable_iab]').on('change',function(){
        var option_name = jQuery(this).attr("id");
        if(jQuery('[name="cli_enable_iab"]').is(':checked') && 'ccpa' !== consentType)
        {
            jQuery('.cli_iab_notice').show();
            jQuery('.wt_cli_enable_consent_mode').show();
            var wt_iab_val = 'true';
            jQuery('.cli_theme_customize .cli_theme_customizebutton , .cli_theme_customize .cli_theme_customizebox , .cli_theme_customize div textarea').css({'opacity':0.5,'pointer-events':'none'});
        }
        else{
            jQuery('.cli_iab_notice').hide();
            jQuery('.wt_cli_enable_consent_mode').hide();
            var wt_iab_val = 'false';
        }
        jQuery.ajax({
            url: cli_admin.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: '&option_name=' + option_name + '&wt_cli_option_val=' + wt_iab_val + '&action=wt_cli_enable_or_disable_iab&_wpnonce=' + cli_admin.nonce,
            success: function (data)
            {
                
            },
        });
    });
    // GCM Support
    jQuery('.cli_gcm_mapping_link').hide();
    jQuery('#cli_gcm_category_modal').hide();

    // Open the modal when the cli_gcm_mapping_link is clicked
    jQuery('.cli_gcm_mapping_link').on('click', function () {
        jQuery('#cli_gcm_category_modal').show();
    });
    // Close the modal when the close button or outside the modal is clicked
    jQuery('.cli_close, #cli_cancel_btn').on('click', function () {
        jQuery('#cli_gcm_category_modal').hide();
    });
    // Prevent the modal from closing when clicking inside the modal content
    jQuery('.cli_modal-content').on('click', function (e) {
        e.stopPropagation();
    });

    if( jQuery('[name="google_consent_mode_field"]').is(':checked') ) {
        jQuery('.cli_gcm_mapping_link').show();
    }
    jQuery('input[type=checkbox][name=google_consent_mode_field]').on('change',function(){
        var option_name = jQuery(this).attr("id");
        if(jQuery('[name="google_consent_mode_field"]').is(':checked')) {
            jQuery('#cli_gcm_category_modal').show();
            jQuery('.cli_gcm_mapping_link').show();
        } else{
            jQuery('.cli_gcm_mapping_link').hide();
        }
    });
    $('.cli_gcm_category').change(function() {
        // Check if the selected value is not empty
        if ($(this).val() !== '0') {
            // Remove the border color
            $(this).css('border', '');
            // Hide the warning icon
            $(this).next('.wt-cli-warning-icon').hide();
            if ($(this).next('.wt-cli-warning-icon').length) {
                $(this).next('.wt-cli-warning-icon').remove();
            }
        }
    });
    // Add your logic to handle the submit button click event
    $('#cli_submit_btn').on('click', function () {
        // Check if the button has already been clicked
        if ($(this).hasClass('clicked')) {
            return; // If clicked, prevent further action
        }
        // Add 'clicked' class to indicate the button has been clicked
        $(this).addClass('clicked');
        // Add opacity to the action buttons
        $('.cli_gcm_action_btn').css('opacity', '0.5');
        // Create an object to store category values
        var category_data = {};
        var hasEmptyCategory = false;
        var icon_url = cli_admin.warning_icon;
        // Loop through all selects with the class 'cli_gcm_category'
        $('.cli_gcm_category').each(function () {
            // Get the ID of the select
            var categoryId = $(this).attr('id');
            // Get the selected categories key of the current select
            var categoryKey = $('#' + categoryId).val();
            // Get the selected categories title of the current select
            var category = $('#' + categoryId + ' option:selected').data('tag-key');

            // Check if the category is not selected
            if (categoryKey === '0') {
                hasEmptyCategory = true;
                // Add a red border to the select box with an empty category
                $('#' + categoryId).css('border', '1px solid red');
                // Check if the warning image is not already appended
                if (!$(this).next('.wt-cli-warning-icon').length) {
                    // Add warning icon before the select box
                    $('#' + categoryId).after('<img class="wt-cli-warning-icon" src="' + icon_url + '">');
                }
            } else {
                // Remove the red border if a category is selected
                $('#' + categoryId).css('border', ''); // Set to empty to remove the border
                 // Remove the warning icon if category is not 0
                $('#' + categoryId).next('.wt-cli-warning-icon').remove();
            }

            // Add the categoryKey and its value to the category_data object
            category_data[category] = categoryKey;
        });

        // Check if any category is not selected
        if (hasEmptyCategory) {
            // Remove 'clicked' class to allow subsequent clicks
            $('#cli_submit_btn').removeClass('clicked');
            // Remove opacity from the action buttons
            $('.cli_gcm_action_btn').css('opacity', '1');
            return; // Prevent form submission
        }

        // Make the AJAX request only if all categories are selected
        jQuery.ajax({
            url: cli_admin.ajax_url,
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'wt_cli_gcm_mapping',
                _wpnonce: cli_admin.nonce,
                option_name: 'cli_gcm_categories',
                wt_cli_option_val: JSON.stringify(category_data), // Convert to JSON string
            },
            success: function (response) {
                jQuery('#cli_gcm_category_modal').hide();
                // Remove 'clicked' class to allow subsequent clicks
                $('#cli_submit_btn').removeClass('clicked');
                // Remove opacity from the action buttons
                $('.cli_gcm_action_btn').css('opacity', '1');
            },
            error: function (error) {
                // Handle error response
                console.error(error);
                // Remove 'clicked' class to allow subsequent clicks
                $('#cli_submit_btn').removeClass('clicked');
                // Remove opacity from the action buttons
                $('.cli_gcm_action_btn').css('opacity', '1');
            },
        });
    });
});