<?php
/*
* Default text for Privacy Generator
*
**/
$cli_pg_default_data=array(
	array(
		'head'=>__( 'About this cookie policy.', 'webtoffee-gdpr-cookie-consent' ),
		'body'=>"",
		'body_file'=>"data.block1.php",
		'status'=>1,
	),
	array(
		'head'=>__( 'What are cookies?', 'webtoffee-gdpr-cookie-consent' ),
		'body'=>"",
		'body_file'=>"data.block2.php",
		'status'=>1,
	),
	array(
		'head'=>__( 'How do we use cookies?', 'webtoffee-gdpr-cookie-consent' ),
		'body'=>"",
		'body_file'=>"data.block3.php",
		'status'=>1,
	),
	array(
		'head'=>__( 'What types of cookies do we use?', 'webtoffee-gdpr-cookie-consent' ),
		'body'=>"",
		'body_file'=>"data.block4.php",
		'status'=>1,
	),
	array(
		'head'=>__( 'How can I control the cookie preferences?', 'webtoffee-gdpr-cookie-consent' ),
		'body'=>"",
		'body_file'=>"data.block5.php",
		'status'=>1,
	),
);