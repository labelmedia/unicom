<?php

/**
 * Settings popup as module for IAB
 *
 * Manages all settings popup customizations for IAB
 *
 * @version 2.5.0
 * @package CookieLawInfo
 */

if (!defined('ABSPATH')) {
    exit;
}
if(!class_exists ( 'Cookie_Law_Info_Settings_Popup_For_IAB' ) ) /* common module class not found so return */
{
    return;
}

class Cookie_Law_Info_Settings_Popup_For_IAB
{
    public function __construct()
    {   
       
    }
    public static function wt_cli_generate_header_html($title,$key,$aria_described_by,$ccpa_category_identity,$checked,$cli_enable_text,$cli_disable_text,$slider_switch)
	{
		if(isset($title))
		{
			?>
            <div class="cli-tab-header cli-<?php echo $key; ?> wt-cli-iab-<?php echo $key; ?>-consents">
                <a id="wt-cli-iab-<?php echo $key; ?>-consents" tabindex="0" role="tab" aria-expanded="false" <?php echo $aria_described_by; ?> aria-controls="wt-cli-tab-<?php echo $key; ?>" class="cli-nav-link cli-settings-mobile" data-target="<?php echo $key; ?>" data-toggle="cli-toggle-tab" style="font-weight: bold;">
                    <?php echo $title; ?>
        		</a>
        		<?php
        		if($slider_switch)
        		{
        			?>
	        		<div class="cli-switch">
	                	<input type="checkbox" class="cli-iab-checkbox wt-cli-consents-checkbox" <?php echo $ccpa_category_identity; ?> id="wt-cli-iab-<?php echo $key; ?>-consents-checkbox" aria-label="<?php echo $key; ?>" data-id="checkbox-<?php echo $key; ?>" role="switch" attr-key ="<?php echo $key; ?>" aria-controls="wt-cli-iab-<?php echo $key; ?>-consents-checkbox" aria-labelledby="wt-cli-tab-link-<?php echo $key; ?>" <?php checked($checked, true); ?> />
	               		<label for="wt-cli-iab-<?php echo $key; ?>-consents-checkbox" class="cli-slider" data-cli-enable="<?php echo $cli_enable_text; ?>" data-cli-disable="<?php echo $cli_disable_text; ?>"><span class="wt-cli-sr-only"><?php echo $key; ?></span></label>
	            	</div>
	            <?php } ?>
    		</div>
        	<?php
		}
	}
	public static function wt_cli_generate_body_html($p_value,$p_key,$key,$aria_described_by,$ccpa_category_identity,$checked,$cli_enable_text,$cli_disable_text,$cli_legitimate_text,$legitimate_interest,$slider_switch)
	{
		if(isset($p_value))
		{
			$title   = ( isset( $p_value['name'] ) ? $p_value['name'] : '' );
			$description   = ( isset( $p_value['description'] ) ? $p_value['description'] : '' );
			$illustration   = ( isset( $p_value['illustrations'] ) ? array_filter($p_value['illustrations']) : '' );
			$vendor_consent_text=__('Number of Vendors seeking consent:','webtoffee-gdpr-cookie-consent');
			?>
            <div class="cli-tab-section cli-sub-tab-section">
				<div class="cli-sub-tab-header cli-<?php echo $key; ?>-<?php echo $p_key; ?>">
                    <a id="wt-cli-iab-<?php echo $key; ?>-consents-item-<?php echo $p_key; ?>" tabindex="0" role="tab" aria-expanded="false" <?php echo $aria_described_by; ?> aria-controls="wt-cli-tab-<?php echo $p_key; ?>" class="cli-nav-link cli-settings-mobile" data-target="<?php echo $p_key; ?>" data-toggle="cli-toggle-tab" >
                        <?php echo $title; ?>
            		</a>
            		<?php
	        		if ( $legitimate_interest ) {

	        			$vendor_consent_text=__('Number of Vendors seeking consent or relying on legitimate interest:','webtoffee-gdpr-cookie-consent');
		        		?>
		        		<div class="cli-switch cli-legitimate-switch">
		                	<input type="checkbox" class="cli-iab-checkbox cli-<?php echo $key; ?>-checkbox" <?php echo $ccpa_category_identity; ?> id="wt-cli-iab-<?php echo $key; ?>-legitimate-interests-checkbox-item-<?php echo $p_key; ?>" aria-label="<?php echo $p_key; ?>" data-id="checkbox-legitimate-<?php echo $key; ?>-<?php echo $p_key; ?>" role="switch" aria-controls="wt-cli-iab-<?php echo $key; ?>-consents-checkbox-legitimate-interests-item-<?php echo $p_key; ?>" aria-labelledby="wt-cli-tab-link-legitimate-<?php echo $key; ?>-<?php echo $p_key; ?>" <?php checked($checked, true); ?> />
		               		<label for="wt-cli-iab-<?php echo $key; ?>-legitimate-interests-checkbox-item-<?php echo $p_key; ?>" class="cli-slider" data-cli-enable="<?php echo $cli_legitimate_text; ?>" data-cli-disable="<?php echo $cli_legitimate_text; ?>"><span class="wt-cli-sr-only"><?php echo $p_key; ?></span></label>
		            	</div>
		        		<?php
		        	}
		        	
	        		if($slider_switch)
	        		{
	        			?>
	            		<div class="cli-switch">
	                    	<input type="checkbox" class="cli-iab-checkbox cli-<?php echo $key; ?>-checkbox" <?php echo $ccpa_category_identity; ?> id="wt-cli-iab-<?php echo $key; ?>-consents-checkbox-item-<?php echo $p_key; ?>" aria-label="<?php echo $key; ?>" data-id="checkbox-<?php echo $key; ?>-<?php echo $p_key; ?>" role="switch" aria-controls="wt-cli-iab-<?php echo $key; ?>-consents-checkbox-item-<?php echo $p_key; ?>" aria-labelledby="wt-cli-iab-<?php echo $key; ?>-consents-checkbox-item-<?php echo $p_key; ?>" <?php checked($checked, true); ?> />
	                   		<label for="wt-cli-iab-<?php echo $key; ?>-consents-checkbox-item-<?php echo $p_key; ?>" class="cli-slider" data-cli-enable="<?php echo $cli_enable_text; ?>" data-cli-disable="<?php echo $cli_disable_text; ?>"><span class="wt-cli-sr-only"><?php echo $p_key; ?></span></label>
	                	</div>
	                	<?php 
	               }?>
            	</div>
				<div class="cli-sub-tab-content" id="wt-cli-iab-<?php echo $key; ?>-consents-sub-content-tab-<?php echo $p_key; ?>">
					<div id="wt-cli-iab-<?php echo $key; ?>-consents-content-<?php echo $p_key; ?>" tabindex="0" role="tabpanel" aria-labelledby="wt-cli-iab-<?php echo $key; ?>-consents-item-<?php echo $p_key; ?>" class="cli-tab-pane cli-fade wt-cli-iab-<?php echo $key; ?>-consents-item-<?php echo $p_key; ?>" data-id="<?php echo $key; ?>-<?php echo $p_key; ?>">
                        <div class="wt-cli-cookie-description"><?php echo do_shortcode( $description, 'cookielawinfo-category' ); ?></div>
                    </div>
                    <div>
                    	
                    	<?php if ( !empty($illustration) && is_array($illustration) ) {
                    		?>
                    		<label class="cli-tab-content-illustration-header"><?php _e('Illustrations', 'webtoffee-gdpr-cookie-consent'); ?></label>
	                    	<ul class="cli-iab-illustrations-des">
	                    		<?php
	                    		foreach ($illustration as $k => $vl) {
	                        		?>
	                              	<li>
	                                <?php esc_html_e($vl, 'webtoffee-gdpr-cookie-consent'); ?>
	                              	</li>
	                             	<?php 
	                         	} ?>
	                        </ul>
                        <?php } ?>
                    </div>
                    <div class="cli-no-of-vendor-consent">
                    	<label><?php echo esc_html_e($vendor_consent_text, 'webtoffee-gdpr-cookie-consent'); ?></label>
                    	<span class="wt-cli-vendors-seek-count"></span>
                    </div>
				</div>
			</div>
        	<?php
		}
	}

	public static function wt_cli_generate_vendor_body_html($p_value,$p_key,$key,$aria_described_by,$ccpa_category_identity,$checked,$cli_enable_text,$cli_disable_text,$cli_legitimate_text,$legitimate_interest,$slider_switch,$key_arr) {
		if( isset( $p_value ) ) {
			$title   = ( isset( $p_value['name'] ) ? $p_value['name'] : '' );
			$purposes   = ( isset( $p_value['purposes'] ) ? array_filter($p_value['purposes']) : array() );
			$legIntPurposes = ( isset( $p_value['legIntPurposes'] ) ? array_filter($p_value['legIntPurposes']) : array() );
			if( !empty( $legIntPurposes ) ) {
				$legitimate_interest = true;
			} else {
				$legitimate_interest = false;
			}
			?>
            <div class="cli-tab-section cli-sub-tab-section">
				<div class="cli-sub-tab-header wt-cli-iab-vendor-consents-item-<?php echo $p_key; ?>">
                    <a id="wt-cli-iab-vendor-consents-item-<?php echo $p_key; ?>" tabindex="0" role="tab" aria-expanded="false" <?php echo $aria_described_by; ?> aria-controls="wt-cli-tab-<?php echo $p_key; ?>" class="cli-nav-link cli-settings-mobile" data-target="<?php echo $key; ?>-<?php echo $p_key; ?>" data-toggle="cli-toggle-tab">
                        <?php echo $title; ?>
            		</a>

            		<?php
	        		if ( $legitimate_interest ) {
		        		?>
		        		<div class="cli-switch cli-legitimate-switch">
		                	<input type="checkbox" class="cli-iab-checkbox cli-vendors-checkbox" <?php echo $ccpa_category_identity; ?> id="wt-cli-iab-vendor-legitimate-interests-checkbox-item-<?php echo $p_key; ?>" aria-label="<?php echo $p_key; ?>" data-id="checkbox-legitimate-<?php echo $key; ?>-<?php echo $p_key; ?>" role="switch" aria-controls="wt-cli-iab-vendor-legitimate-interests-checkbox-item-<?php echo $p_key; ?>" aria-labelledby="wt-cli-iab-vendor-legitimate-interests-checkbox-item-<?php echo $p_key; ?>" <?php checked($checked, true); ?> />
		               		<label for="wt-cli-iab-vendor-legitimate-interests-checkbox-item-<?php echo $p_key; ?>" class="cli-slider" data-cli-enable="<?php echo $cli_legitimate_text; ?>" data-cli-disable="<?php echo $cli_legitimate_text; ?>"><span class="wt-cli-sr-only"><?php echo $p_key; ?></span></label>
		            	</div>
		        		<?php
		        	}
            		
	        		if($slider_switch && !empty( $purposes ))
	        		{
	        			?>
	            		<div class="cli-switch">
	                    	<input type="checkbox" class="cli-iab-checkbox cli-vendors-checkbox" <?php echo $ccpa_category_identity; ?> id="wt-cli-iab-vendor-consents-checkbox-item-<?php echo $p_key; ?>" aria-label="<?php echo $key; ?>-<?php echo $p_key; ?>" data-id="checkbox-<?php echo $key; ?>-<?php echo $p_key; ?>" role="switch" aria-controls="wt-cli-tab-link-<?php echo $key; ?>-<?php echo $p_key; ?>" aria-labelledby="wt-cli-tab-link-<?php echo $p_key; ?>" <?php checked($checked, true); ?> />
	                   		<label for="wt-cli-iab-vendor-consents-checkbox-item-<?php echo $p_key; ?>" class="cli-slider" data-cli-enable="<?php echo $cli_enable_text; ?>" data-cli-disable="<?php echo $cli_disable_text; ?>"><span class="wt-cli-sr-only"><?php echo $p_key; ?></span></label>
	                	</div>
	                	<?php 
	               }?>
            	</div>
				<div class="cli-sub-tab-content">
					<div id="wt-cli-iab-vendor-consents-content-<?php echo $p_key; ?>" tabindex="0" role="tabpanel" aria-labelledby="wt-cli-iab-vendor-consents-item-<?php echo $p_key; ?>" class="cli-tab-pane cli-fade" data-id="<?php echo $key; ?>-<?php echo $p_key; ?>">
						<div class="wt-cli-iab-vendor-detail-section"></div>
						<div class="wt-cli-iab-vendor-storage-disclosure-section"></div>
                    </div>
				</div>
			</div>
							
        	<?php
		}
	}
	/**
	* @since    2.5.0
	* return excluded vendors array
	*/
	
	public static function get_excluded_vendor_list() {
		return apply_filters('wt_cli_excluded_vendor_list',array());
	}
	/**
	* @since    2.5.6
	* return excluded google vendors array
	*/
	
	public static function get_excluded_google_vendor_list() {
		return apply_filters('wt_cli_excluded_google_vendor_list',array());
	}
	/**
	* @since    2.5.0
	* return allowed vendors key array
	*/

	public static function get_allowed_vendor_list()
	{
		$allowed_array    = array();
		$vendor_key_arr   = array();
		$vendors_list_arr = Cookie_Law_Iab_Tcf::get_vendor_list();
		foreach ($vendors_list_arr as $key => $value) {

			if('vendors' === $key)
    		{	
    			$vendors_arr=$value;
    		}
		}
		if(!empty($vendors_arr))
	    {
	        foreach ($vendors_arr as $kl => $kv) {
	        	 
				$vendor_key_arr[]=$kl;
	        }

	    }
	    $excl_vendors_arr = self::get_excluded_vendor_list();
	    if( !empty($excl_vendors_arr) && !empty($vendor_key_arr) )
	    {
	    	$allowed_vendor_key_arr = array_diff($vendor_key_arr, $excl_vendors_arr);
	    }
	    else{
	    	$allowed_vendor_key_arr = $vendor_key_arr;
	    }
		$allowed_vendor_key_arr = apply_filters('wt_cli_allowed_vendor_list',$allowed_vendor_key_arr);
		return array_values($allowed_vendor_key_arr);
	}
	/**
	* @since    2.5.6
	* return allowed google vendors key array
	*/

	public static function get_allowed_google_vendor_list() {
		$google_vendor_key_arr   = array();
		$allowed_vendor_key_arr   = array();
		$googlevendors = Cookie_Law_Iab_Tcf::get_google_vendors();
		if( !empty( $googlevendors ) ) {
			foreach ( $googlevendors as $provider_key => $provider ) {
				if( 0 !== $provider_key ) {
					$google_vendor_key_arr[] = $provider[0];
				}
			}
	    }
	    $excl_vendors_arr = self::get_excluded_google_vendor_list();
	    if( !empty( $excl_vendors_arr ) && !empty( $google_vendor_key_arr ) )  {
	    	$allowed_vendor_key_arr = array_diff( $google_vendor_key_arr, $excl_vendors_arr );
	    }
	    else{
	    	$allowed_vendor_key_arr = $google_vendor_key_arr;
	    }
		$allowed_vendor_key_arr = apply_filters('wt_cli_allowed_google_vendor_list',$allowed_vendor_key_arr);
		return array_values($allowed_vendor_key_arr);
	}
}   

new Cookie_Law_Info_Settings_Popup_For_IAB();