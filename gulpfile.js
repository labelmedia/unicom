const config = {
    src: {
        images: 'img/**/*.{png,jpg,jpeg,gif,svg,ico,json,twig,xml}',
        views: 'web/app/themes/crunch/**/*.{html,phtml,php,twig}'
    },
    dist: {
        dir: 'web/app/themes/unicom/dist'
    },
    options: {
        browsersync: {
            proxy: 'unicom.test'
        }
    }
};

module.exports = require('lynchburg')(config);

