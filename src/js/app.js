import './components/foundation';
import slick from 'slick-carousel';

import HeroSlider from './components/hero-slider';
import GallerySlider from './components/gallery-slider';
import PostsSlider from './components/posts-slider';
import MobileSlider from './components/mobile-slider';
import Menu from './components/menu';
import Dropkick from './components/dropdown';
import ContactRedirect from './components/contact-redirect';
import AOS from './components/aos';
import PlaceholderFix from './components/gf-login-placeholder-fix';
import Rellax from './components/rellax';
import ScrollUp from './components/scroll-up';
import ScrollDown from './components/scroll-down';
import WidgetContainer from './components/widget-container';

jQuery($ => {

    jQuery(document).ready(function () {
        let menu = new Menu();
        let heroSlider = new HeroSlider('.js-hero-slider');
        let gallerySlider = new GallerySlider('.js-gallery-slider');
        let postSlider = new PostsSlider('.js-posts-slider');
        let dropdown = new Dropkick();
        let contactRedirect = new ContactRedirect();
        let aos = new AOS();
        let placeholderFix = new PlaceholderFix();
        let rellax = new Rellax();
        let scrollUp = new ScrollUp();
        let scrollDown = new ScrollDown();

        jQuery('.js-widget-container').each((index, slider) => {
            let widgetContainer = new WidgetContainer(slider);
        });
    });

});
