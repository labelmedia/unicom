import AOS from 'aos';

export default class Aos {

    constructor() {
        AOS.init();
    }

}