import jQuery from 'jquery';

export default class ScrollUp {
    constructor() {
        this.init();
    }

    init() {
        this.events();
    }

    events() {
        jQuery('.js-scroll-down').on('click', (e) => {
            e.preventDefault();

            let heroHeight = jQuery('.js-hero').outerHeight();
            let heroTopOffset = jQuery('.js-hero').offset().top;
            // let widgetHeight = jQuery('.js-hero__widget').outerHeight();

            jQuery('html, body').animate({
                // scrollTop: heroHeight + heroTopOffset + (widgetHeight / 2),
                scrollTop: heroHeight + heroTopOffset,
                duration: this.transitionTime
            })
        });
    }
}
