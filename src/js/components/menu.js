import jQuery from 'jquery';

export default class Menu {

    constructor() {
        this.isLoggedIn = jQuery('.js-body').hasClass('logged-in');

        this.init();
    }

    init() {
        let self = this;

        jQuery('.js-menu-icon').on('click', function(e){
            e.preventDefault();

            if (jQuery('.js-menu').hasClass('is-active')){
                self.closeNav();
            } else {
                self.openNav('navigation');
            }
        });

        jQuery('.js-menu__close').on('click', function(e){
            e.preventDefault();

            self.closeNav();
        });

        // form submission trigger
        jQuery('.js-menu__submit').on('click', function(e){
            e.preventDefault();

            let container = jQuery(this).data('form');
            let $form = jQuery(container).find('form');

            $form.trigger('submit');
        });

        jQuery('.js-member-icon').on('click', function(e){
            if (!self.isLoggedIn) {
                e.preventDefault();
                self.openNav('login');
            }
        });

        jQuery('.js-body').on('click', function(e){
            self.closeNav();
        });
        
        jQuery('.js-menu, .js-member-icon, .js-menu-icon').on('click', function(e) {
            e.stopPropagation();
        });
    }

    closeNav() {
        if (jQuery('.js-menu.is-active').length) {
            window.history.pushState('', '', location.origin + location.pathname + location.hash);
        }
        
        jQuery('.js-menu.is-active').removeClass('is-active');
        jQuery('.js-body').removeClass('utl-disable-y-scroll');
    }

    openNav(type) {
        this.closeNav();

        jQuery('.js-menu').filter('[data-menu=' + type + ']').addClass('is-active');
        jQuery('.js-body').addClass('utl-disable-y-scroll');

        window.history.pushState('', '', '?menu=' + type);
    }

}