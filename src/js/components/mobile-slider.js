import jQuery from 'jquery';

export default class MobileSlider {
    constructor(slider) {
        this.slider = jQuery(slider);

        this.init();
    }

    init() {
        this.update();

        let self = this;

        jQuery(window).on('resize', function() {
            self.update();
        });
    }

    update() {
        if (this.isMobile()){
            this.fireSlider();
        } else {
            this.destroySlider();
        }
    }
    
    isMobile() {
        return window.matchMedia('(max-width: 640px)').matches;
    }

    isInit() {
        return this.slider.hasClass('slick-initialized');
    }

    fireSlider() {
        if (!this.isInit()) {
            this.slider.slick({
                arrows: false,
                dots: false,
                autoplay: true,
                autoplaySpeed: 3000,
                autoHeight: true
            })
        }
    }

    destroySlider() {
        if (this.isInit()){
            this.slider.slick('unslick');
        }
    }
}