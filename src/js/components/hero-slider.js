import jQuery from 'jquery';

export default class HeroSlider {

    constructor(slider) {
        this.slider = slider;

        this.init();
    }

    init() {
        jQuery(this.slider).slick({
            arrows: true,
            dots: true
        });
    }

}