import jQuery from 'jquery';

import { Foundation } from 'foundation-sites/dist/js/plugins/foundation.core';

import { Accordion } from 'foundation-sites/dist/js/plugins/foundation.accordion';
import { Keyboard } from 'foundation-sites/dist/js/plugins/foundation.util.keyboard';
import { MediaQuery } from 'foundation-sites/dist/js/plugins/foundation.util.mediaQuery';
// import { Motion } from 'foundation-sites/dist/js/plugins/foundation.util.motion';
import { Tabs } from 'foundation-sites/dist/js/plugins/foundation.tabs';
// import { Touch } from 'foundation-sites/dist/js/plugins/foundation.util.touch';
// import { Triggers } from 'foundation-sites/dist/js/plugins/foundation.util.triggers';

import { OffCanvas } from 'foundation-sites/dist/js/plugins/foundation.offcanvas';
    // import { ResponsiveAccordionTabs } from 'foundation-sites/dist/js/plugins/foundation.responsiveAccordionTabs';
import { Reveal } from 'foundation-sites/dist/js/plugins/foundation.reveal';
// import { SmoothScroll } from 'foundation-sites/dist/js/plugins/foundation.smoothScroll';

Foundation.addToJquery(jQuery);

Foundation.plugin(Accordion, 'Accordion');
Foundation.plugin(Keyboard, 'Keyboard');
// Foundation.plugin(MediaQuery, 'MediaQuery');
// Foundation.plugin(Motion, 'Motion');
Foundation.plugin(Tabs, 'Tabs');
// Foundation.plugin(Touch, 'Touch');
// Foundation.plugin(Triggers, 'Triggers');

// Foundation.plugin(OffCanvas, 'OffCanvas');
// Foundation.plugin(ResponsiveAccordionTabs, 'ResponsiveAccordionTabs');
Foundation.plugin(Reveal, 'Reveal');
// Foundation.plugin(SmoothScroll, 'SmoothScroll');

jQuery($ => {
    $(document).foundation();
});
