import jQuery from 'jquery';
import Swiper from 'swiper';

export default class HeroSlider {

    constructor(slider) {
        this.slider = jQuery(slider).find('.js-gallery-slider__slide')[0];
        this.widgets = jQuery('.js-gallery-slider__widget');
        this.nextButton = jQuery(slider).find('.js-gallery-slider__next');
        this.prevButton = jQuery(slider).find('.js-gallery-slider__prev');
        this.progressBar = jQuery(slider).find('.js-gallery-slider__progress-bar');

        this.init();
    }

    init() {
        let self = this;

        self.swiperSlider = new Swiper(this.slider, {
            direction: 'horizontal',
            slidesPerView: 1,
            loop: false,
            breakpoints: {
                1024: {
                    slidesPerView: 1.5,
                    spaceBetween: 20,
                    centeredSlides: true
                }
            }
        });

        self.events();
    }

    events() {
        let self = this;

        self.nextButton.on('click', function(e){
            e.preventDefault();

            self.swiperSlider.slideNext();
        });

        self.prevButton.on('click', function(e){
            e.preventDefault();

            self.swiperSlider.slidePrev();
        });

        self.swiperSlider.on('slideChangeTransitionEnd', function(){
            let index = this.realIndex + 1;

            self.onSlideUpdate(index);
        });
    }

    onSlideUpdate(index) {
        let self = this;

        let slidesCount = parseInt(self.slider.dataset.count);

        console.log(index);

        self.widgets.not('[data-index=' + index + ']').addClass('is-hidden');
        self.widgets.filter('[data-index=' + index + ']').removeClass('is-hidden');

        let percentage = index / slidesCount * 100;

        self.progressBar.css('width', percentage + '%');
    }


}