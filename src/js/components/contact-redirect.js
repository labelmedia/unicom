import jQuery from 'jquery';

export default class ContactRedirect {
    
    constructor() {
        console.log('Init');
        this.init()
    }

    init() {
        let self = this;

        // gravity forms contact dropdown
        jQuery('#input_2_1').on('change', function(){
            let value = jQuery(this).val();

            self.redirect(value);
        });
    }

    redirect(value) {
        location.href = '/contact/?choice=' + value;
    }

}