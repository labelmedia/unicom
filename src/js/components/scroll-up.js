import jQuery from 'jquery';

export default class ScrollUp {
    constructor() {
        this.scrollElement = jQuery('.js-scroll-up');
        this.transitionTime = 200;

        this.init();
    }

    init() {
        this.test();
        this.events();
    }

    events() {
        jQuery(window).on('scroll', () => {
            this.test()
        });

        this.scrollElement.on('click', (e) => {
            e.preventDefault();

            jQuery('html, body').animate({
                scrollTop: 0,
                duration: 400
            })
        });
    }

    isTop() {
        return jQuery(window).scrollTop() > 0;
    }

    test() {
        if (this.isTop()){
            this.scrollElement.fadeIn(this.transitionTime);
        } else {
            this.scrollElement.fadeOut(this.transitionTime);
        }
    }
}