import jQuery from 'jquery';

export default class WidgetContainer {
    constructor(selector) {
        this.widgetContainer = jQuery(selector);
        this.widget = this.widgetContainer.find('.js-widget');

        // debugger;
        this.init();
    }

    init() {
        this.update();
        this.events();
    }

    events() {
        jQuery(window).on('resize', () => {
            this.update();
        });
    }

    isDesktop() {
        return window.matchMedia('(min-width: 640px)').matches; 
    }

    update() {
        if (this.isDesktop()){
            let minusMargin = this.widget.outerHeight() / 2;

            this.widgetContainer.css('margin-bottom', -Math.abs(minusMargin));
        } else {
            this.widgetContainer.removeAttr('style');
        }
    }
}