// this really is a horrid way of amending the placeholder attributes on the login form
// but time is money, and I have neither
// if YOU have time, the plugin is https://www.gravityforms.com/add-ons/user-registration/
// just need to find a way to add the placeholder attribute through a filter or setting of some sort

import jQuery from 'jquery';

export default class GravityFormsPlaceholderFix {

    constructor() {
        this.init();
    }

    init() {
        jQuery('#gform_fields_login .gfield_label').each(function(){
            let label = jQuery(this).text();
            let input = jQuery(this).next('.ginput_container').find('input');

            input.attr('placeholder', label);
        });
    }

}