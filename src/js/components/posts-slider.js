import jQuery from 'jquery';
import Swiper from 'swiper';

export default class PostsSlider {
    constructor(slider) {
        this.slider = jQuery(slider).find('.js-posts-slider__slide')[0];
        this.nextButton = jQuery(slider).find('.js-posts-slider__next');
        this.prevButton = jQuery(slider).find('.js-posts-slider__prev');
        this.progressBar = jQuery(slider).find('.js-posts-slider__progress-bar');

        this.init();
    }

    init() {
        let self = this;

        self.swiperSlider = new Swiper(this.slider, {
            loop: false,
            slidesPerView: 1,
            spaceBetween: 20,
            direction: 'horizontal',
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                    autoHeight: false
                },
                1024: {
                    slidesPerView: 2.4,
                    spaceBetween: 30,
                    autoHeight: false
                }
            }
        });

        self.events();
    }

    events() {
        let self = this;

        self.nextButton.on('click', function(e){
            e.preventDefault();

            self.swiperSlider.slideNext();
        });

        self.prevButton.on('click', function(e){
            e.preventDefault();

            self.swiperSlider.slidePrev();
        });

        self.swiperSlider.on('slideChangeTransitionEnd', function(){
            let index = this.realIndex + 1;

            self.onSlideUpdate(index);
        });
    }

    onSlideUpdate(index) {
        let self = this;

        let slidesCount = parseInt(self.slider.dataset.count);

        let percentage = index / slidesCount * 100;

        self.progressBar.css('width', percentage + '%');
    }
}