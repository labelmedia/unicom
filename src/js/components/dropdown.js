import jQuery from 'jquery';
import 'dropkickjs/dist/dropkick';

export default class Dropkick {

    constructor() {
        this.init();
    }

    triggerChangeEvent() {
        let input = document.getElementById('input_6_6_1');
        let self = this;

        if (input !== null){
            let event = new Event('change', {
                bubbles: true
            });

            input.dispatchEvent(event);
        }
    }

    fire() {
        let self = this;

        jQuery('select').dropkick({
            mobile: true
        });

        jQuery('select').dropkick('refresh');
    }

    init() {
        let self = this;

        self.triggerChangeEvent();

        window.initDropdowns = function() {
            self.fire();
        }
    }

}