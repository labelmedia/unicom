from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

my_url = "https://www.totalcorner.com/match/today"

# opening up connection, grabbing the page
uClient = uReq(my_url)
page_html = uClient.read()
uClient.close()

# html parsing
page_soup = soup(page_html, "html.parser")
containers1 = page_soup.findAll("div")
print(len(containers1))
# containers2 = page_soup.findAll("table", {"class": "marketboard-event-with-
# header__markets-list"})
# print(len(containers2))